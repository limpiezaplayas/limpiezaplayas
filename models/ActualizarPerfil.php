<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ActualizarPerfil extends Model
{
    public $nombre;
    public $apellidos;
    public $foto;
    public $imageFile;
    public $ubicacion;

    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'ubicacion'], 'string', 'max' => 100, 'tooLong' => 'El {attribute} no puede tener más de 100 caracteres.'],
            [['nombre', 'apellidos', 'ubicacion'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/', 'message' => 'El {attribute} solo puede contener letras.'],
            [['foto'], 'string', 'max' => 255],
            [
                ['imageFile'], 'file', 'skipOnEmpty' => true,
                'extensions' => 'png, jpg, jpeg, webp', 'maxSize' => 1024 * 1024 * 2,
                'tooBig' => 'El tamaño de la imagen no puede exceder los 2MB.'
            ], // Tamaño máximo 2MB
        ];
    }

    public function upload($currentPhoto)
    {
        if ($this->validate() && $this->imageFile) {
            $directory = Yii::getAlias('@webroot') . '/img/upload-user/';
            if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
            }
            $filePath = $directory . $this->imageFile->baseName . '.' . $this->imageFile->extension;

            if ($this->imageFile->saveAs($filePath)) {
                // Elimina la imagen anterior si no es la imagen por defecto
                $defaultPhoto = 'img/upload-user/photo-default.webp';
                if ($currentPhoto && $currentPhoto != $defaultPhoto) {
                    @unlink(Yii::getAlias('@webroot') . '/' . $currentPhoto);
                }

                $this->foto = 'img/upload-user/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
                return true;
            }
        }
        return false;
    }
}
