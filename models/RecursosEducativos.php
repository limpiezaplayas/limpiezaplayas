<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "recursos_educativos".
 *
 * @property int $id
 * @property string $titulo
 * @property string $descripcion
 * @property string|null $enlace_recurso
 *
 * @property UsuariosRecursos[] $usuariosRecursos
 */
class RecursosEducativos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recursos_educativos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'descripcion'], 'required'],
            [['titulo'], 'string', 'max' => 300, 'tooLong' => 'El título debe contener un máximo de 300 caracteres.'],
            [['titulo'], 'match', 'pattern' => '/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\/\-\s()]+$/', 'message' => 'El título solo puede contener letras, números, espacios y los caracteres / - ().'],
            [['descripcion'], 'string', 'max' => 500, 'tooLong' => 'La descripción debe contener un máximo de 500 caracteres.'],
            [['descripcion'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s.()]+$/', 'message' => 'La descripción solo puede contener letras, espacios, puntos y paréntesis.'],
            [['enlace_recurso'], 'string', 'max' => 300],
            [['enlace_recurso'], 'url', 'defaultScheme' => 'http', 'message' => 'El enlace debe ser una URL válida.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'enlace_recurso' => 'Enlace Recurso',
        ];
    }

    /**
     * Gets query for [[UsuariosRecursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuariosRecursos()
    {
        return $this->hasMany(UsuariosRecursos::class, ['recurso_educativo_id' => 'id']);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RecursosEducativos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'enlace_recurso', $this->enlace_recurso]);

        return $dataProvider;
    }
}
