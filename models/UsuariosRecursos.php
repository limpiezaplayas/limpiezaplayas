<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios_recursos".
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $recurso_educativo_id
 *
 * @property RecursosEducativos $recursoEducativo
 * @property Usuarios $usuario
 */
class UsuariosRecursos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios_recursos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id', 'recurso_educativo_id'], 'required'],
            [['usuario_id', 'recurso_educativo_id'], 'integer'],
            [['recurso_educativo_id'], 'exist', 'skipOnError' => true, 'targetClass' => RecursosEducativos::class, 'targetAttribute' => ['recurso_educativo_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'recurso_educativo_id' => 'Recurso Educativo ID',
        ];
    }

    /**
     * Gets query for [[RecursoEducativo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecursoEducativo()
    {
        return $this->hasOne(RecursosEducativos::class, ['id' => 'recurso_educativo_id']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::class, ['id' => 'usuario_id']);
    }
}
