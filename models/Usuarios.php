<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string|null $ubicacion
 * @property string $nombre
 * @property string $apellidos
 * @property string $password
 * @property string $correo
 * @property string $rol
 * @property int|null $foros_id
 *
 * @property Donaciones[] $donaciones
 * @property Eventos[] $eventos
 * @property Foros $foros
 * @property Telefono[] $telefonos
 * @property UsuariosRecursos[] $usuariosRecursos
 */
class Usuarios extends \yii\db\ActiveRecord
{
    const ROL_ADMIN = 'administrador';
    const ROL_USUARIO = 'usuario';

    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'password', 'correo'], 'required'],
            [['foros_id'], 'integer'],
            [['ubicacion', 'nombre', 'correo', 'rol'], 'string', 'max' => 50],
            [['apellidos'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 255],
            [['correo'], 'unique'],
            [['foros_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blog::class, 'targetAttribute' => ['foros_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ubicacion' => 'Ubicacion',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'password' => 'Password',
            'correo' => 'Correo',
            'rol' => 'Rol',
            'foros_id' => 'Foros ID',
        ];
    }

    /**
     * Gets query for [[Donaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonaciones()
    {
        return $this->hasMany(Donaciones::class, ['usuario_id' => 'id']);
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::class, ['usuario_id' => 'id']);
    }

    /**
     * Gets query for [[Foros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getForos()
    {
        return $this->hasOne(Blog::class, ['id' => 'foros_id']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefono::class, ['usuario_id' => 'id']);
    }

    /**
     * Gets query for [[UsuariosRecursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuariosRecursos()
    {
        return $this->hasMany(UsuariosRecursos::class, ['usuario_id' => 'id']);
    }

       public function esAdmin()
    {
       
        return $this->rol === self::ROL_ADMIN; 
    }
}
