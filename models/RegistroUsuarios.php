<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string|null $ubicacion
 * @property string $nombre
 * @property string $apellidos
 * @property string $username
 * @property string $password
 * @property string $correo
 * @property string $rol
 * @property int|null $foros_id
 *
 * @property Eventos[] $eventos
 * @property Foros $foros
 * @property Telefono[] $telefonos
 * @property UsuariosRecursos[] $usuariosRecursos
 */
class RegistroUsuarios extends \yii\db\ActiveRecord
{
    public $confirmarPassword;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'password', 'correo', 'confirmarPassword'], 'required', 'message' => 'El campo {attribute} es obligatorio.'],
            [['ubicacion', 'nombre', 'correo', 'rol'], 'string', 'max' => 50],
            [['apellidos'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 255],
            [['correo'], 'unique'],

            /* VALIDACIONES */
            [['nombre', 'apellidos'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑü\s]+$/', 'message' => '{attribute} solo puede contener letras.'],
            [['nombre', 'apellidos'], 'string', 'min' => 3, 'tooShort' => '{attribute} debe tener al menos 3 caracteres.'],
            ['correo', 'match', 'pattern' => '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/', 'message' => 'El formato de {attribute} no es válido.'],
            ['password', 'match', 'pattern' => '/^.{6,}$/', 'message' => 'La {attribute} debe tener al menos 6 caracteres.'],
            ['confirmarPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Las contraseñas no coinciden'],

        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ubicacion' => 'Ubicacion',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'password' => 'Contraseña',
            'correo' => 'Correo',
            'rol' => 'Rol',
            'foros_id' => 'Foros ID',
        ];
    }



    /**
     * Registra un nuevo usuario.
     *
     * @param array $data Los datos del nuevo usuario.
     * @return bool True si el registro fue exitoso, false en caso contrario.
     */

    public function registrarUsuario($data)
    {
        $this->username = $data['username'];
        $this->correo = $data['correo'];
        $this->password = $data['password']; 

        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }



    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Eliminar los espacios al inicio y al final de los campos nombre y apellidos
            $this->nombre = trim($this->nombre);
            $this->apellidos = trim($this->apellidos);

            // Si el campo de la contraseña ha sido modificado, aseguramos que no sea nulo
            if ($this->isAttributeChanged('password') && !empty($this->password)) {
                $this->password = trim($this->password);
            }

            // Establecer foto predeterminada si está vacía
            if (empty($this->foto)) {
                $this->foto = 'img/upload-user/photo-default.webp';
            }
        }
        return parent::beforeSave($insert);
    }
}
