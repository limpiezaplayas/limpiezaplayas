<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "playas".
 *
 * @property int $id
 * @property string $nombre
 * @property string $ubicacion
 * @property string $historial_limpieza
 *
 * @property Eventos[] $eventos
 */
class Playas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'playas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'ubicacion', 'historial_limpieza'], 'required'],
            [['nombre', 'ubicacion'], 'string', 'max' => 50],
            [['historial_limpieza'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'ubicacion' => 'Ubicacion',
            'historial_limpieza' => 'Historial Limpieza',
        ];
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::class, ['playas_id' => 'id']);
    }
}
