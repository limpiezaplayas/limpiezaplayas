<?php

namespace app\models;

use Yii;
use app\models\Usuarios;

/**
 * This is the model class for table "donaciones".
 *
 * @property int $id
 * @property int|null $usuario_id
 * @property string|null $nombre
 * @property string|null $apellido
 * @property string|null $correo
 * @property float $monto
 * @property string $fecha
 *
 * @property Usuarios $usuario
 */
class Donaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

     public $apellidos;

    public static function tableName()
    {
        return 'donaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id'], 'integer'],
            [['monto', 'nombre', 'apellido', 'correo'], 'required', 'message' => '{attribute} no puede estar vacío.'],
            [['monto'], 'number', 'min' => 1, 'max' => 999999, 'tooSmall' => 'El monto debe ser mayor a cero.', 'tooBig' => 'El monto no puede tener más de 6 dígitos.'],
            [['fecha'], 'safe'],
            [['nombre', 'apellido', 'correo'], 'string', 'max' => 255],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::class, 'targetAttribute' => ['usuario_id' => 'id']],
            [['nombre', 'apellido'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/u', 'message' => '{attribute} solo puede contener letras.'],
            [['nombre', 'apellido'], 'string', 'max' => 100, 'tooLong' => '{attribute} no puede tener más de 100 caracteres.'],
            [['correo'], 'email', 'message' => 'El formato de {attribute} no es válido.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'correo' => 'Correo',
            'monto' => 'Monto',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::class, ['id' => 'usuario_id']);
    }

    public function validarMaximoDigitos($attribute, $params)
    {
        $maxDigitos = 6;
        $monto = $this->$attribute;
        $montoStr = strval($monto);
        if (strlen($montoStr) > $maxDigitos) {
            $this->addError($attribute, 'El monto no puede tener más de 6 dígitos.');
        }
    }
}
