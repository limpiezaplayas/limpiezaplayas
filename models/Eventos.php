<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "eventos".
 *
 * @property int $id
 * @property string|null $fecha_evento_creado
 * @property string|null $fecha_evento
 * @property string|null $hora_evento
 * @property string|null $descripcion
 * @property string|null $foto
 * @property string $estado_evento
 * @property int|null $usuario_id
 * @property int|null $playas_id
 *
 * @property Playas $playas
 * @property Usuarios $usuario
 */
class Eventos extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_evento_creado', 'fecha_evento', 'hora_evento'], 'safe'],
            [['estado_evento'], 'required'],
            [['estado_evento'], 'string'],
            [['usuario_id', 'playas_id'], 'integer'],
            [['descripcion', 'foto'], 'string', 'max' => 500],
            [['playas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Playas::class, 'targetAttribute' => ['playas_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::class, 'targetAttribute' => ['usuario_id' => 'id']],
            [['foto'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, webp', 'maxSize' => 1024 * 1024 * 2, 'tooBig' => 'El tamaño máximo permitido es de 2MB.'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, webp', 'maxSize' => 1024 * 1024 * 2, 'tooBig' => 'El tamaño máximo permitido es de 2MB.'],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_evento_creado' => 'Fecha Evento Creado',
            'fecha_evento' => 'Fecha Evento',
            'hora_evento' => 'Hora Evento',
            'descripcion' => 'Descripcion',
            'foto' => 'Foto',
            'estado_evento' => 'Estado Evento',
            'usuario_id' => 'Usuario ID',
            'playas_id' => 'Playas ID',
            'imageFile' => 'Imagen del Evento',
        ];
    }

    /**
     * Gets query for [[Playas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlayas()
    {
        return $this->hasOne(Playas::class, ['id' => 'playas_id']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::class, ['id' => 'usuario_id']);
    }

    /**
     * Upload image and return its relative path.
     *
     * @return string|false the relative path of the uploaded image or false if upload failed
     */
    public function upload()
    {
        if ($this->validate() && $this->imageFile) {
            $directory = Yii::getAlias('@webroot') . '/img/upload-event/';
            if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
            }

            $filePath = $directory . $this->imageFile->baseName . '.' . $this->imageFile->extension;

            if ($this->imageFile->saveAs($filePath)) {
                $this->foto = 'img/upload-event/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
                return true;
            }
        }
        return false;
    }
}
