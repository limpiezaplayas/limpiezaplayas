<?php

namespace app\models;

use app\models\User;

use Yii;
use yii\base\Model;


/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user
 *
 */
class LoginForm extends Model
{
    public $correo;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // correo and password son requeridos
            [['correo', 'password'], 'required', 'message' => 'El campo {attribute} no puede estar en blanco.'],

            // Salida error correo
            ['correo', 'email', 'message' => 'El formato de {attribute} no es válido.'],

            // Validación correo
            ['correo', 'match', 'pattern' => '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/', 'message' => 'El formato de {attribute} no es válido.'],

            // Validación Password
            ['password', 'string', 'min' => 6, 'max' => 50, 'tooShort' => 'La contraseña debe tener al menos 6 caracteres.', 'tooLong' => 'La contraseña no puede tener más de 50 caracteres.'],

            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'correo' => 'Correo electrónico',
            'password' => 'Contraseña',
            'rememberMe' => 'Recordarme',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            // Verifica si $user no es null antes de llamar a validatePassword()
            if ($user !== null && $user->validatePassword($this->password)) {
                // La contraseña es válida
            } else {
                $this->addError($attribute, 'Correo electrónico o contraseña incorrectos.');
            }
        }
    }


    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {

        if ($this->validate()) {
            // Obtén el usuario utilizando el nombre de usuario proporcionado en el formulario
            $user = $this->getUser();

            // Verifica si el usuario existe y si la contraseña proporcionada es válida
            if ($user && $user->validatePassword($this->password)) {
                // Inicia sesión con el usuario
                return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            }
        }

        return false;
    }


    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->correo);
        }

        return $this->_user;
    }
}
