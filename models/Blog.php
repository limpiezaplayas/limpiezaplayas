<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class Blog extends ActiveRecord
{
    public $imageFile;

    public static function tableName()
    {
        return 'blog';
    }

    public function rules()
    {
        return [
            [['titulo', 'descripcion', 'categoria'], 'required'],
            [['titulo'], 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'El título solo puede contener letras y espacios.'],
            [['descripcion'], 'string', 'max' => 500],
            [['fecha_creacion'], 'default', 'value' => date('Y-m-d H:i:s')],
            [['fecha_creacion'], 'safe'],
            [['imagen'], 'required', 'on' => 'create'],
            [['categoria'], 'in', 'range' => ['voluntariado', 'medio ambiente', 'conservacion']],
            [['imagen'], 'file', 'extensions' => ['png', 'jpg', 'jpeg', 'webp'], 'maxSize' => 1024 * 1024 * 2, 'tooBig' => 'El tamaño máximo permitido es de 2MB.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'fecha_creacion' => 'Fecha Creacion',
            'categoria' => 'Categoria',
            'imagen' => 'Imagen',
        ];
    }

    public function upload()
    {
        if ($this->validate() && $this->imageFile instanceof UploadedFile) {
            $directory = Yii::getAlias('@webroot') . '/img/upload-blog/';
            if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
            }

            $filePath = $directory . Yii::$app->security->generateRandomString() . '.' . $this->imageFile->extension;

            if ($this->imageFile->saveAs($filePath)) {
                return '/img/upload-blog/' . basename($filePath);
            }
        }
        return false;
    }

    public static function getAvailableCategories()
    {
        return self::find()
            ->select(['categoria', 'COUNT(*) AS count'])
            ->groupBy('categoria')
            ->orderBy('categoria')
            ->asArray()
            ->all();
    }

    public static function getAllBlogs()
    {
        return self::find()->all();
    }

    public function getUsuarios()
    {
        return $this->hasMany(User::class, ['foro_id' => 'id']);
    }
}
