<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;


class User extends ActiveRecord implements IdentityInterface
{

    public $authKey;
    public $accessToken;
    public $file;

    // Definir constantes de roles
    const ROL_ADMIN = 'administrador';
    const ROL_USUARIO = 'usuario';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'usuarios';
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }


    /**
     * Encuentra un usuario por su dirección de correo electrónico
     * @param string $email
     * @return User|null
     */

    public static function findByEmail($email)
    {
        $user = static::findOne(['correo' => $email]);
        if ($user !== null) {
            // Asignar el hash de la contraseña al modelo User
            $user->password = $user->password;
        }
        return $user;
    }

    /* 
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
    */


    /**
     * Validates a password.
     *
     * @param string $password the password to validate
     * @return bool whether the password is valid
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }


    /**
     * Verificar si el usuario es administrador
     * @return bool
     */
    public function esAdmin()
    {

        return $this->rol === self::ROL_ADMIN;
    }
}
