/* VARIABLES */

/* EVENTOS */
document.addEventListener("DOMContentLoaded", function () {
  clonarLogos();
  animateSuccess();
  showText();
  showListLink();
  defaultPrice();
  updateTotalPrice();
  modal();
  paraLLax();
});

/*  FUNCIONES - METODOS */
function clonarLogos() {
  var logosSlide = document.querySelector(".logos-slide");

  if (logosSlide) {
    var copy = logosSlide.cloneNode(true);

    var logosContainer = document.querySelector(".logos");

    if (logosContainer) {
      logosContainer.appendChild(copy);
    }
  }
}

/* ACCDER AL DOM - NO SE PUEDE DESDE YII PARA APLICAR ANIMACIÓN */
function animateSuccess() {
  var buttonSuccess = document.querySelector(".alert-success");

  if (buttonSuccess) {
    buttonSuccess.classList.add("animate__animated");
    buttonSuccess.classList.add("animate__fadeInDown");
    buttonSuccess.classList.add("rounded-pill");
  }
}

function showText() {
  document.querySelectorAll(".item").forEach((item) => {
    item.addEventListener("click", () => {
      // Oculta todos los textos de referencia
      document.querySelectorAll(".texto-info").forEach((texto) => {
        texto.classList.remove("mostrar");
      });

      // Elimina la clase 'seleccionado' de todos los elementos de la lista
      document.querySelectorAll(".item").forEach((item) => {
        item.classList.remove("seleccionado");
      });

      // Encuentra el texto de referencia correspondiente y muéstralo
      const textoId = "texto-" + item.id;
      const texto = document.getElementById(textoId);
      texto.classList.add("mostrar");

      // Añade la clase 'seleccionado' al elemento de la lista seleccionado
      item.classList.add("seleccionado");
    });
  });
}

function showListLink() {
  const enlaces = document.querySelectorAll(".link-dropdown");
  enlaces.forEach(function (enlace) {
    enlace.addEventListener("click", function (event) {
      event.preventDefault();
      const texto = this.nextElementSibling;
      if (texto) {
        if (texto.style.display === "block") {
          texto.style.display = "none"; // Si ya está visible, lo ocultamos
        } else {
          texto.style.display = "block"; // Si no está visible, lo mostramos
        }
      }
    });
  });
}

// Nombre del archivo: custom.js

function defaultPrice() {
  var defaultAmountLinks = document.querySelectorAll(".default-amount");
  var otraCantidadButton = document.getElementById("otra-cantidad");

  defaultAmountLinks.forEach(function (link) {
    link.addEventListener("click", function (event) {
      event.preventDefault();
      var amount = this.getAttribute("data-amount");
      document.getElementById("donaciones-monto").value = amount;
    });
  });

  if (otraCantidadButton) {
    otraCantidadButton.addEventListener("click", function () {
      document.getElementById("donaciones-monto").value = "";
      document.getElementById("donaciones-monto").focus();
    });
  }
}

function updateTotalPrice() {
  var inputMonto = document.querySelector(".input-donar");

  if (inputMonto) {
    inputMonto.addEventListener("input", function () {
      var monto = parseFloat(this.value).toFixed(2);

      document.querySelector(".total .eur").textContent = monto + " €";
    });
  }
}

function modal() {
  var modal = document.getElementById("myModal");
  var span = document.getElementsByClassName("close")[0];
  var images = document.getElementsByClassName("modal-img");

  if (modal && span && images.length > 0) {
    // Función para cerrar el modal
    function closeModal() {
      modal.style.display = "none";
    }

    // Recorrer todas las imágenes y agregar un controlador de eventos de clic
    for (var i = 0; i < images.length; i++) {
      images[i].addEventListener("click", function () {
        // Mostrar el modal
        modal.style.display = "block";
        // Establecer la fuente de la imagen del modal como la fuente de la imagen clicada
        document.getElementById("modalImage").src = this.src;
      });
    }

    // Cuando el usuario haga clic en (x), cerrar el modal
    span.onclick = closeModal;

    // Cuando el usuario presione la tecla Esc, cerrar el modal
    document.onkeydown = function (event) {
      event = event || window.event;
      if (event.keyCode === 27) {
        // 27 es el código para la tecla "Esc"
        closeModal();
      }
    };

    // Cuando el usuario haga clic en cualquier parte fuera del modal, cerrar el modal
    window.onclick = function (event) {
      if (event.target == modal) {
        closeModal();
      }
    };
  }
}

function paraLLax() {
  var parallaxElements = document.querySelectorAll(".parallax-bg");

  parallaxElements.forEach(function (element) {
    var parallaxFactor = element.dataset.parallax || 0.5;

    // Aplicar el efecto de paralaje al elemento
    element.style.backgroundAttachment = "fixed";
    element.style.backgroundPosition = "center";
    element.style.backgroundRepeat = "no-repeat";
    element.style.backgroundSize = "cover";
    element.style.overflow = "hidden";

    // Función que se ejecutará en el evento de desplazamiento
    window.addEventListener("scroll", function () {
      var scrollTop = window.pageYOffset;
      var parallaxOffset = scrollTop * parallaxFactor;

      element.style.backgroundPositionY = parallaxOffset + "px";
    });
  });
}

// Llamar a la función para inicializar el efecto de paralaje
paraLLax();

