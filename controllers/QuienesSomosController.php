<?php

namespace app\controllers;

use yii\web\Controller;

class QuienesSomosController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
