<?php
namespace app\controllers;

use Yii;
use app\models\Donaciones;
use app\models\Blog;
use yii\web\Controller;

class DonacionesController extends Controller
{
    public function actionIndex()
    {
        $model = new Donaciones();

        // Obtener el modelo de usuario actual si está logueado
        $usuario = Yii::$app->user->identity;

        if (!Yii::$app->user->isGuest) {
            $model->nombre = $usuario->nombre;
            $model->apellido = $usuario->apellidos;
            $model->correo = $usuario->correo;
        }

        if ($model->load(Yii::$app->request->post())) {
            // Asignar la fecha actual a la donación
            $model->fecha = date('Y-m-d H:i:s');

            // Validar y guardar la donación
            if ($model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success', '¡Gracias por tu donación!');
                return $this->redirect(['index']);
            }
        }

        $categories = Blog::getAvailableCategories();

        // Obtener los tres últimos blogs
        $ultimosTresBlogs = Blog::find()->orderBy(['fecha_creacion' => SORT_DESC])->limit(3)->all();

        $montoTotalDonado = Donaciones::find()->sum('monto');

        return $this->render('index', [
            'model' => $model,
            'categories' => $categories,
            'ultimosTresBlogs' => $ultimosTresBlogs,
            'montoTotalDonado' => $montoTotalDonado,
        ]);
    }
}
