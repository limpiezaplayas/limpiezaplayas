<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Comentarios;
use yii\web\NotFoundHttpException;

class ComentariosController extends Controller
{
    // ... otros métodos

    public function actionCreate($blog_id)
{
    $model = new Comentarios();

    // Verificar si el formulario ha sido enviado
    if ($model->load(Yii::$app->request->post())) {
        // Asignar el ID del blog al nuevo comentario
        $model->blog_id = $blog_id;
        // Asignar el ID y nombre del usuario actual al nuevo comentario
        $user = Yii::$app->user->identity;
        $model->usuario_id = $user->id;
        $model->nombre_usuario = $user->nombre;
        // Obtener la fecha y hora actual
        $model->fecha_comentario = date('Y-m-d H:i:s');
        // Guardar el comentario en la base de datos
        if ($model->save()) {
            // El comentario se ha guardado correctamente
            Yii::$app->session->setFlash('success', '¡Comentario enviado correctamente!');
        } else {
            // Ocurrió un error al guardar el comentario
            Yii::$app->session->setFlash('error', 'Hubo un problema al guardar el comentario.');
        }
    } else {
        // El formulario no se envió correctamente
        Yii::$app->session->setFlash('error', 'Hubo un problema al enviar el formulario.');
    }

    // Redireccionar a la vista del blog
    return $this->redirect(['blog/view', 'id' => $blog_id]);
}


}

