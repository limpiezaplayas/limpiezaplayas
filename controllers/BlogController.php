<?php

namespace app\controllers;

use app\models\Blog;
use app\models\Comentarios;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use Yii;

class BlogController extends Controller
{
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actionIndex()
    {
        $query = Blog::find();
        $blogs = $query->orderBy(['fecha_creacion' => SORT_DESC])->all();
        $categories = Blog::getAvailableCategories();

        $model = new Blog();
        $user = Yii::$app->user->identity;

        $ultimosTresBlogs = array_slice($blogs, 0, 3);

        $comentariosCount = [];
        foreach ($blogs as $blog) {
            $comentariosCount[$blog->id] = Comentarios::find()->where(['blog_id' => $blog->id])->count();
        }

        return $this->render('index', [
            'blogs' => $blogs,
            'model' => $model,
            'user' => $user,
            'categories' => $categories,
            'ultimosTresBlogs' => $ultimosTresBlogs,
            'comentariosCount' => $comentariosCount,
        ]);
    }

    public function actionView($id)
    {
        $blog = $this->findModel($id);
        $comentarios = Comentarios::find()->where(['blog_id' => $id])->all();
        $comentariosCount = count($comentarios);

        $query = Blog::find();
        $blogs = $query->orderBy(['fecha_creacion' => SORT_DESC])->all();
        $categories = Blog::getAvailableCategories();

        $ultimosTresBlogs = array_slice($blogs, 0, 3);

        return $this->render('view', [
            'blog' => $blog,
            'comentarios' => $comentarios,
            'comentariosCount' => $comentariosCount,
            'categories' => $categories,
            'ultimosTresBlogs' => $ultimosTresBlogs,
        ]);
    }

    public function actionCreate()
    {
        $model = new Blog();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if ($model->validate()) {
                    $rutaImagen = $model->upload();
                    if ($rutaImagen !== false) {
                        $model->imagen = $rutaImagen;
                        if ($model->save()) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $currentImage = $model->imagen;

        if ($this->request->isPost) {
            $postData = $this->request->post();
            $model->load($postData);

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                if ($model->validate() && $rutaImagen = $model->upload()) {
                    $model->imagen = $rutaImagen;
                } else {
                    $model->imagen = $currentImage; 
                }
            } else {
                $model->imagen = $currentImage; 
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Blog::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
