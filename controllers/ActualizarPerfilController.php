<?php
namespace app\controllers;

use Yii;
use app\models\ActualizarPerfil;
use app\models\Usuarios;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

class ActualizarPerfilController extends Controller
{
    public function actionIndex()
    {
        return $this->redirect(['actualizar']);
    }

    public function actionActualizar()
    {
        $model = new ActualizarPerfil();
        $usuario = $this->findModel(Yii::$app->user->identity->id);

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile && $model->upload($usuario->foto)) {
                $usuario->foto = $model->foto;
            }

            if (!empty($model->nombre)) {
                $usuario->nombre = $model->nombre;
            }
            if (!empty($model->apellidos)) {
                $usuario->apellidos = $model->apellidos;
            }
            if (!empty($model->ubicacion)) {
                $usuario->ubicacion = $model->ubicacion;
            }

            if ($usuario->save(false)) {
                Yii::$app->session->setFlash('success', 'Perfil actualizado con éxito.');
                return $this->redirect(['actualizar']);
            }
        }

        // Cargar valores actuales en el formulario
        $model->foto = $usuario->foto;
        $model->nombre = $usuario->nombre;
        $model->apellidos = $usuario->apellidos;
        $model->ubicacion = $usuario->ubicacion;

        return $this->render('index', [
            'model' => $model,
            'usuario' => $usuario,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Usuarios::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('El usuario no existe.');
    }
}
