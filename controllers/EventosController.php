<?php

namespace app\controllers;

use Yii;
use app\models\Eventos;
use app\models\Playas;
use app\models\Blog;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * EventosController implements the CRUD actions for Eventos model.
 */
class EventosController extends Controller
{
    public $imageFile;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Eventos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Eventos::find(),
        ]);

        $eventos = Eventos::find()->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'eventos' => $eventos,
        ]);
    }

    /**
     * Displays a single Eventos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $playa = $model->playas;
    
        // Obtener los últimos eventos, ordenados por fecha en orden descendente
        $ultimosEventos = Eventos::find()
            ->orderBy(['fecha_evento_creado' => SORT_ASC])
            ->limit(3)
            ->all();
    
        return $this->render('view', [
            'model' => $model,
            'playa' => $playa,
            'ultimosEventos' => $ultimosEventos,
        ]);
    }
    




    /**
     * Creates a new Eventos model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest || Yii::$app->user->identity->rol !== 'administrador') {
            throw new ForbiddenHttpException('No tienes permiso para acceder a esta página.');
        }

        $model = new Eventos();

        if ($model->load(Yii::$app->request->post())) {
            $model->fecha_evento_creado = date('Y-m-d H:i:s');
            $model->usuario_id = Yii::$app->user->id;

            // Procesar la imagen subida
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile && !$model->upload()) {
                Yii::$app->session->setFlash('error', 'Error al subir la imagen.');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            // Combinar la fecha y la hora del evento
            if (!empty($model->fecha_evento) && !empty($model->hora_evento)) {
                $fechaHora = \DateTime::createFromFormat('d-m-Y H:i', $model->fecha_evento . ' ' . $model->hora_evento);
                if ($fechaHora) {
                    $model->fecha_evento = $fechaHora->format('Y-m-d H:i:s');
                } else {
                    Yii::$app->session->setFlash('error', 'Fecha y hora del evento no válidas.');
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }

            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Evento creado con éxito.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Error al guardar el evento.');
            }
        }

        $playas = Playas::find()->select(['id', 'nombre', 'ubicacion'])->all();
        $playasList = ArrayHelper::map($playas, 'id', 'nombre');

        return $this->render('create', [
            'model' => $model,
            'playasList' => $playasList,
        ]);
    }








    /**
     * Updates an existing Eventos model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        // Guardar la fecha y hora originales antes de cargar los nuevos datos
        $originalFechaEvento = $model->fecha_evento;
        $originalHoraEvento = $model->hora_evento;

        if ($model->load(Yii::$app->request->post())) {

            $model->fecha_evento_creado = date('Y-m-d H:i:s');
            $model->usuario_id = Yii::$app->user->id;

            // Procesar la imagen subida
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile && !$model->upload()) {
                Yii::$app->session->setFlash('error', 'Error al subir la imagen.');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            // Verificar si la fecha del evento no se modificó, mantener la fecha original
            if (empty($model->fecha_evento)) {
                $model->fecha_evento = $originalFechaEvento;
            } else {
                // Convertir fecha a formato 'Y-m-d'
                $fecha = \DateTime::createFromFormat('d-m-Y', $model->fecha_evento);
                if ($fecha) {
                    $model->fecha_evento = $fecha->format('Y-m-d');
                } else {
                    Yii::$app->session->setFlash('error', 'Fecha del evento no válida.');
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }

            // Verificar si la hora del evento no se modificó, mantener la hora original
            if ($model->hora_evento === $originalHoraEvento || empty($model->hora_evento)) {
                $model->hora_evento = $originalHoraEvento;
            } else {
                // Verificar si la hora es válida
                if (preg_match('/^([01]\d|2[0-3]):([0-5]\d)$/', $model->hora_evento)) {
                    // La hora está en formato válido de 24 horas
                } else {
                    Yii::$app->session->setFlash('error', 'Hora del evento no válida.');
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }

            
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Evento actualizado con éxito.');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', 'Error al guardar el evento.');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }











    /**
     * Deletes an existing Eventos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Eventos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Eventos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Eventos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
