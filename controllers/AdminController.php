<?php

namespace app\controllers;

use app\models\Donaciones;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\models\Eventos;
use app\models\RecursosEducativos;
use app\models\User;

class AdminController extends Controller
{
    // Aplicar filtro de acceso
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'], // Permite el acceso a usuarios autenticados
                        'matchCallback' => function ($rule, $action) {
                            // Verifica si el usuario tiene el rol de administrador
                            return Yii::$app->user->identity->rol === 'administrador';
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Eventos::find(),
            // Opciones adicionales de configuración aquí, como paginación
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionEventos()
    {
        $dataProviderEventos = new ActiveDataProvider([
            'query' => Eventos::find(),
            // Aquí puedes configurar opciones adicionales, como paginación
        ]);

        return $this->render('/eventos/tablaEventos', [
            'dataProvider' => $dataProviderEventos,
        ]);
    }

    public function actionUsuarios()
    {
        $dataProviderUsuarios  = new ActiveDataProvider([
            'query' => User::find(),
            // Aquí puedes configurar opciones adicionales, como paginación
        ]);

        return $this->render('/usuarios/tablaUsuarios', [
            'dataProvider' => $dataProviderUsuarios ,
        ]);
    }

    public function actionRecursos()
    {
        $dataProvideRecursosEducativos  = new ActiveDataProvider([
            'query' => RecursosEducativos::find(),
            // Aquí puedes configurar opciones adicionales, como paginación
        ]);

        return $this->render('/recursos-educativos/tablaRecursosEducativos', [
            'dataProvider' => $dataProvideRecursosEducativos ,
        ]);
    }

    public function actionDonaciones()
    {
        $dataProvideDonaciones  = new ActiveDataProvider([
            'query' => Donaciones::find(),
            // Aquí puedes configurar opciones adicionales, como paginación
        ]);

        return $this->render('/donaciones/tablaDonaciones', [
            'dataProvider' => $dataProvideDonaciones ,
        ]);
    }
}
