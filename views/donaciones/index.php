<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\AppAsset;

$this->title = 'Realizar Donación';
AppAsset::register($this);

$objetivoRecaudacion = 15000.00;

?>

<div class="container container-donacion">
    <div class="row row-1">
        <div class="col-8">
            <div class="img">
                <img src="<?= Yii::getAlias('@web') ?>/img/voluntariadov2.webp" alt="Voluntariado Donación" class="Voluntariado img-fluid">
            </div>
            <h1 class="title">Para la conservación de nuestras playas y la salud de nuestros mares.</h1>
            <p class="mount-donate">
                <span class="eur"><?= number_format($montoTotalDonado, 2, ',', '.') ?> €</span> de <span class="eur"><?= number_format($objetivoRecaudacion, 2, ',', '.') ?> €</span>
            </p>

            <div class="progress">
                <div class="gray"></div>
                <div class="progress-eur" style="width: <?= min(($montoTotalDonado / $objetivoRecaudacion) * 100, 100) ?>%;"></div>
            </div>
        </div>
        <div class="col-4 c-2">
            <div class="b-2">
                <h6 class="title-donacion">Categorías</h6>
                <ul class="p-0 categorias-donar categorias-blog">
                    <?php foreach ($categories as $category) : ?>
                        <li><?= Html::a(Html::encode($category['categoria'] . ' (' . $category['count'] . ')'), '#', ['class' => 'item-b', 'onclick' => 'return false;']) ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <!-- Post Recientes -->
            <div class="b-2 mt-5">
                <h6 class="title-donacion">Blog Recientes</h6>
                <ul class="p-0 categorias-donar categorias-blog blog-reciente">
                    <?php foreach ($ultimosTresBlogs as $blog) : ?>
                        <li class="d-flex align-items-center">
                            <?php if ($blog->imagen) : ?>
                                <div class="img-blog d-flex">
                                    <img src="<?= Yii::getAlias('@web') . $blog->imagen ?>" alt="Imagen del blog" class="img-rec">
                                </div>
                            <?php endif; ?>
                            <h6 class="title-recent-blog title-recent-donacion text-center"><?= Html::encode($blog->titulo) ?></h6>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <!-- MODAL -->
            <div class="b-3 mt-5">
                <h6 class="title-donacion">Galería</h6>
                <?= Html::img('@web/img/ocean.webp', ['class' => 'modal-img', 'alt' => 'Imagen 1']) ?>
                <?= Html::img('@web/img/limp-playa.webp', ['class' => 'modal-img', 'alt' => 'Imagen 2']) ?>
                <?= Html::img('@web/img/voluntariadov2.webp', ['class' => 'modal-img', 'alt' => 'Imagen 3']) ?>
                <?= Html::img('@web/img/limpieza-playa.webp', ['class' => 'modal-img', 'alt' => 'Imagen 1']) ?>
                <?= Html::img('@web/img/voluntariado.png', ['class' => 'modal-img', 'alt' => 'Imagen 2']) ?>
                <?= Html::img('@web/img/ocean.webp', ['class' => 'modal-img', 'alt' => 'Imagen 3']) ?>
                <!-- Modal -->
                <div id="myModal" class="modal">
                    <!-- Contenido del modal -->
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <img id="modalImage" src="" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
        <div class="form-donacion">

<?php $form = ActiveForm::begin(); ?>

<div class="monto">
    <div class="e d-flex align-items-center">
        <span class="input-group-text">€</span>
    </div>
    <?= $form->field($model, 'monto')->textInput([
        'type' => 'number', 
        'required' => true, 
        'class' => 'input-donar', 
        'placeholder' => '100,00',
        'min' => 1,
        'max' => 999999,
        'step' => '1'
    ]) ?>
</div>

<div class="form-group">
    <?= Html::a('10,00 €', '#', ['class' => 'default-amount', 'data-amount' => 10]) ?>
    <?= Html::a('20,00 €', '#', ['class' => 'default-amount', 'data-amount' => 20]) ?>
    <?= Html::a('50,00 €', '#', ['class' => 'default-amount', 'data-amount' => 50]) ?>
    <?= Html::a('100,00 €', '#', ['class' => 'default-amount', 'data-amount' => 100]) ?>
    <?= Html::a('Otra Cantidad', '#', ['id' => 'otra-cantidad', 'class' => 'default-amount']) ?>
</div>

<hr class="mt-5 mb-4">
<span class="info-personal mb-4"><b>Información Personal</b></span>
<div class="names d-flex justify-content-between">
    <?= $form->field($model, 'nombre')->textInput([
        'maxlength' => true,
        'readonly' => !Yii::$app->user->isGuest,
    ]) ?>

    <?= $form->field($model, 'apellido')->textInput([
        'maxlength' => true,
        'readonly' => !Yii::$app->user->isGuest,
    ]) ?>
</div>

<?= $form->field($model, 'correo')->textInput([
    'type' => 'email',
    'readonly' => !Yii::$app->user->isGuest,
]) ?>

<div class="donacion-total my-4 d-flex">
    <div class="e d-flex align-items-center">
        <span class="input-group-text">Donación Total:</span>
    </div>
    <div class="total d-flex align-items-center">
        <span class="eur">100,00 €</span>
    </div>
</div>

<div class="form-group mb-0">
    <?= Html::submitButton('Donar', ['class' => 'btn btn-clean btn-donar']) ?>
</div>

<?php ActiveForm::end(); ?>
</div>



        </div>
    </div>

    <div class="row">
        <div class="col-8">
            <h3 class="mt-5">Introducción</h3>
            <p>¡Bienvenido a nuestra página dedicada a promover el voluntariado en las playas! Creemos firmemente en la importancia de preservar nuestras costas y mares, y en el acceso equitativo a la naturaleza para todos. Al unirte a nuestra causa, puedes contribuir de manera significativa a la protección de nuestras playas y la vida marina, así como a fomentar la conciencia ambiental y el disfrute responsable de estos espacios. ¡Tu participación es clave para crear un impacto positivo y duradero en nuestro entorno costero!</p>
        </div>
    </div>

    <div class="row">
        <div class="col-8">
            <h3 class="mt-5">Impacto de nuestra causa</h3>
            <p> El voluntariado en playas es una forma efectiva de abordar este desafío al tiempo que se protege y conserva uno de nuestros recursos más preciados: el entorno marino. Al unirte a nuestra aplicación de voluntariado en playas, puedes marcar la diferencia al proporcionar alivio inmediato a quienes enfrentan estas dificultades y contribuir a construir soluciones sostenibles para la resiliencia a largo plazo de nuestras costas y la salud de nuestros mares.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-8">
            <h3 class="mt-5">Cómo funcionan tus donaciones:</h3>
            <p>
                Tus donaciones nos permiten colaborar con organizaciones locales y comunidades costeras para llevar a cabo actividades de limpieza, conservación y educación en nuestras playas. Priorizamos iniciativas que promueven la conciencia ambiental y empoderan a las comunidades para proteger y preservar nuestros preciosos entornos costeros. Cada contribución, sin importar su tamaño, tiene un impacto significativo en la salud de nuestras playas y en el bienestar de quienes dependen de ellas.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-8">
            <h3 class="mt-5">¡Gracias a ti! Siempre es un placer ayudar</h3>
            <p>
                Gracias por considerar apoyar la conservación de nuestras playas y la salud de nuestros mares. Tu generosidad tiene el poder de preservar estos preciosos entornos costeros y garantizar que las futuras generaciones puedan disfrutar de ellos. Juntos, podemos marcar la diferencia y asegurar que nuestras playas sigan siendo un lugar seguro y hermoso para todos.
            </p>
        </div>
    </div>

</div>