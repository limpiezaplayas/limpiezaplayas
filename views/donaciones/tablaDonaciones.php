<?php

use app\models\Donaciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Donaciones';
?>
<div class="donaciones-index">

    <h1 class="tabla-h1"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Donación', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-dark table-hover'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'apellido',
            'correo',
            'monto',
            'fecha',
            //'foros_id',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Donaciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>