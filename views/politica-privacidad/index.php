<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Política de Privacidad';
$this->params['breadcrumbs'][] = ['label' => 'Política de Privacidad', 'url' => ['index']];
?>

<div class="aviso-legal">
    <div class="container">
        <div class="row my-5">
            <div class="col">
                <h1><?= Html::encode($this->title) ?></h1>

                <h5 class="mt-5">Introducción</h5>
                <p>
                    La presente Política de Privacidad ha sido desarrollada teniendo en cuenta lo dispuesto por la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y Garantía de los Derechos Digitales (LOPD-GDD), así como por el Reglamento 2016/679 del Parlamento Europeo y del consejo del 27 de abril de 2016 relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la circulación de estos datos, en adelante el RGPD.
                </p>
                <p>
                    Esta Política de Privacidad tiene por objeto poner en conocimiento de los titulares de los datos personales, respecto de los cuales se está recabando información, los aspectos específicos relativos al tratamiento sus datos, entre otras cosas, las finalidades de los tratamientos, los datos de contacto para ejercer los derechos que le asisten, los plazos de conservación de la información y las medidas de seguridad entre otras cosas.
                </p>

                <h5 class="mt-5">Responsable del tratamiento</h5>

                <p>
                    En términos de protección de datos FUNDACIÓN ECOPLAYAS, debe ser considerado Responsable del Tratamiento, con relación a los ficheros/tratamientos identificados en la presente política, concretamente en el apartado Tratamientos de datos.
                    A continuación, se indican los datos identificativos del titular del presente sitio web:
                    <br><br>


                    <strong>Responsable del Tratamiento:</strong> FUNDACIÓN ECOPLAYAS<br>
                    <strong>NIF:</strong> G12345678<br>
                    <strong>Dirección</strong> postal: CALLE Prueba 1, 39001 Santander - CANTABRIA<br>
                    <strong>Dirección electrónica:</strong> infoventanasdelmundo@gmail.com<br>
                </p>

                <h5 class="mt-5">Tratamientos de datos</h5>
                <p>
                    Los datos de carácter personal que se soliciten, en su caso, consistirán únicamente en aquellos estrictamente imprescindibles para identificar y atender la solicitud realizada por el titular de estos, en adelante el interesado. Por otra parte, los datos personales serán recogidos para finalidades determinadas explícitas y legítimas, no siendo tratados ulteriormente de manera con incompatible con dichos fines
                </p>
                <p>
                    Los datos recogidos de cada interesado serán adecuados, pertinentes y no excesivos con relación a las finalidades correspondientes para cada caso, y serán actualizados siempre que sean necesarios.
                </p>
                <p>
                    El titular de los datos será informado, con carácter previo a la recogida de sus datos, de los extremos generales en esta política a fin de que pueda prestar el consentimiento expreso, preciso e inequívoco para el tratamiento de sus datos, conforme a los siguientes aspectos.
                </p>

                <h5 class="mt-5">Finalidades del tratamiento</h5>
                <p>
                    Las finalidades explícitas para las cuales se llevan a cabo cada uno de los tratamientos vienen recogido en las cláusulas informativas incorporadas en cada una de las vías de toma de datos (formularios web, formularios en papel, locuciones o carteles y notas informativas).
                </p>
                <p>
                    No obstante, los datos de carácter personal del interesado serán tratados con la exclusiva finalidad de proporcionarles una respuesta efectiva y atender las solicitudes practicadas por el usuario, especificadas junto a la opción, servicio, formulario o sistema de toma de datos que el titular utilice.
                </p>

                <h5 class="mt-5">Legitimación</h5>
                <p>
                    Por regla general, previo al tratamiento de los datos personales, FUNDACIÓN ECOPLAYAS. obtiene consentimiento expreso e inequívoco del titular de estos, mediante la incorporación de cláusulas de consentimiento informado en los diferentes sistemas de recogida de información.
                </p>
                <p>
                    No obstante, en caso de que no se requiera el consentimiento del interesado, la base legitimadora del tratamiento en la cual se ampara FUNDACIÓN VENTANAS DEL MUNDO. es la existencia de una ley o norma específica que autorice o exija el tratamiento de los datos del interesado.
                </p>
                <h5 class="mt-5">Destinatarios</h5>
                <p>
                    Por regla general, FUNDACIÓN ECOPLAYAS no procede a la cesión o comunicación de los datos a terceras entidades, salvo las requeridas legalmente, no obstante, en caso de que fuera necesario, dichas cesiones o comunicaciones de datos se informan al interesado a través de cláusulas de consentimiento informado contenidas en las diferentes vías de recogida de datos personales.
                </p>

                <h5 class="mt-5">Procedencia</h5>
                <p>
                    Por regla general, los datos personales se recogen siempre directamente del interesado, no obstante, en determinadas excepciones, los datos pueden ser recogidos a través de terceras personas, entidades o servicios diferentes del interesado. En este sentido, este extremo será trasladado al interesado a través de las cláusulas de consentimiento informado contenidas en las diferentes vías de recogida de información y dentro de un plazo razonable, una vez obtenidos los datos, y a más tardar dentro de un mes.
                </p>

                <h5 class="mt-5">Plazos de conservación</h5>
                <p>
                    La información recabada del interesado será conservada mientras sea necesaria para cumplir con la finalidad para la cual fueron recabados los datos personales, de forma que, una vez cumplida la finalidad los datos serán cancelados. Dicha cancelación dará lugar al bloqueo de los datos conservándose únicamente a disposición de las APP, Jueces y Tribunales, para atender las posibles responsabilidades nacidas del tratamiento, durante el plazo de prescripción de éstas, cumplido el citado plazo se procederá a la destrucción de la información.
                </p>
                <h5 class="mt-5">Seguridad</h5>
                <p>
                    Las medidas de seguridad adoptadas por FUNDACIÓN ECOPLAYAS. son aquellas requeridas, de conformidad con los establecidos en el artículo 32 del RGPD. En este sentido, FUNDACIÓN ECOPLAYAS., teniendo en cuenta el estado de la técnica, los costes de aplicación y la naturaleza, el alcance, el contexto y los fines del tratamiento, así como los riesgos de probabilidad y gravedad variables para los derechos y las libertades de las personas físicas, tienen establecidas las medidas técnicas y organizativas apropiadas para garantizar el nivel de seguridad adecuado al riesgo existente.
                </p>
            </div>
        </div>
    </div>
</div>