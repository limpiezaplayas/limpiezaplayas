<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Política de Cookies';
$this->params['breadcrumbs'][] = ['label' => 'Política de Cookies', 'url' => ['index']];
?>

<div class="aviso-legal">
    <div class="container">
        <div class="row my-5">
            <div class="col">
                <h1><?= Html::encode($this->title) ?></h1>

                <h5 class="mt-5">¿Qué es una cookie?</h5>
                <p>
                    Las cookies son pequeños archivos que algunas plataformas, como las páginas web, pueden instalar en tu ordenador. Sus funciones pueden ser muy variadas: almacenar tus preferencias de navegación, recopilar información estadística, permitir ciertas funcionalidades técnicas... En ocasiones, las cookies se utilizan para almacenar información básica sobre los hábitos de navegación del usuario o de su equipo, hasta el punto, según los casos, de poder reconocerlo.
                </p>


                <h5 class="mt-5">¿Por qué son importantes?</h5>

                <p>
                    Las cookies son útiles por varios motivos. Desde un punto de vista técnico, permiten que las páginas web funcionen de forma más ágil y adaptada a tus preferencias, como por ejemplo almacenar tu idioma o la moneda de tu país. Además, ayudan a los responsables de los sitios web a mejorar sus servicios, gracias a la información estadística que recogen a través de ellas. Finalmente, sirven para hacer más eficiente la publicidad que te mostramos, gracias a la cual te podemos ofrecer nuestros servicios de forma gratuita.
                </p>

                <h5 class="mt-5">¿Qué tipos de cookies utiliza esta web?</h5>
                <p>
                    En esta página utilizamos 3 tipos de cookies:
                </p>

                <p>
                    <strong>Cookies de sesión:</strong> son cookies temporales que permanecen en el archivo de cookies de tu navegador hasta que abandones la página web, por lo que ninguna queda registrada en el disco duro del usuario. La información obtenida por medio de estas cookies, sirven para analizar pautas de tráfico en la web. A la larga, esto nos permite proporcionar una mejor experiencia para mejorar el contenido y facilitando su uso.
                </p>
                <p>
                    <strong>Cookies de análisis:</strong> Son aquéllas que bien tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado
                </p>
                <p>
                    <strong>Cookies técnicas y de personalización:</strong> Son aquéllas que permiten al usuario la navegación a través de la página web, plataforma o aplicación y la utilización de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, realizar el proceso de compra de un regalo, utilizar elementos de seguridad durante la navegación, idioma de navegación, etc.
                </p>


                <h5 class="mt-5">Consentimiento</h5>

                <p>
                    Al navegar y continuar en el sitio web estará consintiendo el uso de las cookies antes enunciadas y en las condiciones contenidas en la presente Política de Cookies.
                    Puede obtener más información sobre nosotros a través de nuestro apartado de "Información Legal".
                </p>

                <h5 class="mt-5">¿Qué implica no autorizar el uso de cookies?</h5>

                <p>
                    Si usted rechaza la instalación de cookies en su terminal de usuario o, si elimina las que se hubieran registrado en otros accesos anteriores, debe tener en cuenta que no podrá beneficiarse de una serie de utilidades que son necesarias para navegar en algunas zonas de nuestra página web. Así, no podrá reconocer, por ejemplo, el tipo de navegador instalado en el equipo terminal de usuario para poder adaptar la visualización de la página web.
                </p>

                <h5 class="mt-5">¿Cómo las puedo configurar?</h5>

                <p>
                    Puedes permitir, bloquear o eliminar las cookies instaladas en tu equipo mediante la configuración de las opciones de tu navegador de Internet. A continuación te explicamos cómo:<br>
                <ul>
                    <li>
                        <?= Html::a(
                            'Google Chrome',
                            'https://support.google.com/chrome/answer/95647?hl=es',
                            [
                                'target' => '_blank',
                                'class' => 'cookies'
                            ]
                        ) ?>
                    </li>

                    <li>
                        <?= Html::a(
                            'Mozilla Firefox',
                            'https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we&redirectlocale=es',
                            [
                                'target' => '_blank',
                                'class' => 'cookies'
                            ]
                        ) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            'Internet Explorer',
                            'https://support.microsoft.com/es-es/windows/eliminar-y-administrar-cookies-168dab11-0753-043d-7c16-ede5947fc64d',
                            [
                                'target' => '_blank',
                                'class' => 'cookies'
                            ]
                        ) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            'Safari',
                            'https://support.apple.com/kb/ph5042?locale=es_ES&viewlocale=es_es',
                            [
                                'target' => '_blank',
                                'class' => 'cookies'
                            ]
                        ) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            'Safari para iOs (iPhone, iPad)',
                            'https://support.apple.com/es-es/105082',
                            [
                                'target' => '_blank',
                                'class' => 'cookies'
                            ]
                        ) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            'Chrome para Android',
                            'https://support.google.com/chrome/answer/114662?hl=es&visit_id=638503384817352975-1782069718&rd=1',
                            [
                                'target' => '_blank',
                                'class' => 'cookies'
                            ]
                        ) ?>
                    </li>
                </ul>



                
                
                
                </p>
            </div>
        </div>
    </div>
</div>