<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'Blog';

/* CONVIERTE LA FECHA DE FORMATO A INGLES A ESPAÑOL */
setlocale(LC_TIME, 'es_ES.UTF-8');

?>

<div class="row row-banner">
    <div class="col banner-container p-0">
        <h1 class="title"><?= Html::encode($this->title) ?></h1>
        <div class="overlay"></div>
        <?= Html::img('@web/img/banner1.webp', ['alt' => 'Banner Img Logo', 'class' => 'img-banner']) ?>
    </div>
</div>

<div class="blog-index w-100">

    <!-- Botón para crear un nuevo blog (SI EL USUARIO LOGEADO ES ADMIN) -->
    <div class="row my-3 row-blog">
        <div class="col p-0 py-4">
            <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'administrador') : ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= Html::a('Crear Blog', ['create'], ['class' => 'btn btn-clean']) ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <!-- TARJETAS - CARDS -->
    <div class="row row-2 row-blog">
        <div class="col-8 container-card">
            <?php foreach ($blogs as $blog) : ?>
                <div class="card mb-4 shadow-sm">
                    <div class="row no-gutters card-c">
                        <div class="col-md-4">
                            <?php if ($blog->imagen) : ?>
                                <?php
                                $rutaImagen = Yii::getAlias('@web') . $blog->imagen;
                                ?>
                                <img src="<?= $rutaImagen ?>" class="card-img" alt="Imagen del blog">
                            <?php endif; ?>
                        </div>

                        <div class="col-md-8">
                            <div class="card-body">
                                <!-- BOTON CATEGORIA-->
                                <a href="<?= Url::to(['blog/view', 'id' => $blog->id]) ?>" class="btn btn-b btn-category">
                                    <p class="card-text">
                                        <small><?= Html::encode($blog->categoria) ?></small>
                                    </p>
                                </a>
                                <!-- FECHA -->
                                <p class="card-text date">
                                    <i class="fas fa-calendar-alt"></i> <?= strftime('%e %B, %Y', strtotime($blog->fecha_creacion)) ?>&nbsp;&nbsp;
                                    <i class="fa-regular fa-comment"></i><?= isset($comentariosCount[$blog->id]) ? $comentariosCount[$blog->id] : 0 ?>&nbsp;
                                </p>

                                <h5 class="card-title"><?= Html::encode($blog->titulo) ?></h5>
                                <p class="card-text"><?= Html::encode($blog->descripcion) ?></p>

                                <a href="<?= Url::to(['blog/view', 'id' => $blog->id]) ?>" class="btn btn-b">Saber más</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="col-4 c-2">
            <div class="b-2">
                <h6>Categorías</h6>
                <ul class="p-0 categorias-donar categorias-blog">
                    <?php foreach ($categories as $category) : ?>
                        <li><?= Html::a(Html::encode($category['categoria'] . ' (' . $category['count'] . ')'), ['/ruta/a/' . strtolower($category['categoria'])], ['class' => 'item-b']) ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <!-- Post Recientes -->
            <div class="b-2 mt-5">
                <h6>Blog Recientes</h6>
                <ul class="p-0 categorias-donar categorias-blog blog-reciente">
                    <?php foreach ($ultimosTresBlogs as $blog) : ?>
                        <li class="d-flex align-items-center">
                            <?php if ($blog->imagen) : ?>
                                <div class="img-blog d-flex">
                                    <img src="<?= Yii::getAlias('@web') . $blog->imagen ?>" alt="Imagen del blog" class="img-rec">
                                </div>
                            <?php endif; ?>
                            <h6 class="title-recent-blog text-center"><?= Html::encode($blog->titulo) ?></h6>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="b-3 mt-5">
                <h6>Galería</h6>
                <?= Html::img('@web/img/ocean.webp', ['class' => 'modal-img', 'alt' => 'Imagen 1']) ?>
                <?= Html::img('@web/img/limp-playa.webp', ['class' => 'modal-img', 'alt' => 'Imagen 2']) ?>
                <?= Html::img('@web/img/voluntariadov2.webp', ['class' => 'modal-img', 'alt' => 'Imagen 3']) ?>
                <?= Html::img('@web/img/limpieza-playa.webp', ['class' => 'modal-img', 'alt' => 'Imagen 1']) ?>
                <?= Html::img('@web/img/voluntariado.png', ['class' => 'modal-img', 'alt' => 'Imagen 2']) ?>
                <?= Html::img('@web/img/ocean.webp', ['class' => 'modal-img', 'alt' => 'Imagen 3']) ?>
                <!-- Modal -->
                <div id="myModal" class="modal">
                    <!-- Contenido del modal -->
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <img id="modalImage" src="" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>