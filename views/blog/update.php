<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Actualizar Blog';
?>

<div class="blog-update m-4 container">
    <div class="row justify-content-center">
        <div class="col-6 col-form-blog-create">
            <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'enableClientValidation' => true, 'enableAjaxValidation' => false]); ?>

            <!-- Campo para mostrar la vista previa de la imagen -->
            <div class="profile-picture-container event-picture-container text-center">
                <div class="profile-picture event-picture">
                    <div class="image-preview">
                        <?= Html::img($model->imagen ? '@web/' . $model->imagen : '@web/img/upload-event/event-default.jpg', [
                            'alt' => 'Imagen del evento',
                            'class' => 'img-update-event',
                            'id' => 'event-picture-preview',
                        ]) ?>
                        <div class="upload-button">
                            <?= Html::label('<i class="fas fa-camera"></i>', 'event-picture-input', ['class' => 'btn btn-upload']) ?>
                            <?= $form->field($model, 'imageFile')->fileInput([
                                'id' => 'event-picture-input',
                                'class' => 'd-none',
                            ])->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Titulo -->
            <?= $form->field($model, 'titulo')->textInput(['maxlength' => true, 'pattern' => '[a-zA-Z\s]+', 'title' => 'Solo letras y espacios.', 'class' => 'form-control w-100']) ?>

            <!-- Descripción -->
            <div class="form-group">
                <?= $form->field($model, 'descripcion')->textarea(['rows' => 6, 'maxlength' => 500, 'id' => 'descripcion'])->label('Descripción') ?>
                <small id="char-count" class="form-text text-muted">0/500 caracteres</small>
            </div>

            <!-- Categoría -->
           
            <?= $form->field($model, 'categoria')->dropDownList([
                        'voluntariado' => 'Voluntariado',
                        'medio ambiente' => 'Medio Ambiente',
                        'conservacion' => 'Conservación'
                    ], ['prompt' => 'Seleccionar categoría', 'options' => [$model->categoria => ['Selected' => true]]]) ?>


            <div class="form-group">
                <?= Html::submitButton('Actualizar', ['class' => 'btn btn btn-clean w-100']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var textarea = document.getElementById('descripcion');
        var charCount = document.getElementById('char-count');

        // Inicializar el contador de caracteres al cargar la página
        var textLength = textarea.value.length;
        charCount.textContent = textLength + '/500 caracteres';

        // Actualizar el contador de caracteres en tiempo real
        textarea.addEventListener('input', function() {
            textLength = textarea.value.length;
            charCount.textContent = textLength + '/500 caracteres';
        });

        // Previsualización de la imagen
        document.getElementById('event-picture-input').addEventListener('change', function(event) {
            var preview = document.getElementById('event-picture-preview');
            var file = event.target.files[0];
            var reader = new FileReader();

            reader.onloadend = function() {
                preview.src = reader.result;
            };

            if (file) {
                reader.readAsDataURL(file);
            }
        });
    });
</script>
