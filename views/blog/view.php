<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Comentarios;

// Inicializar un nuevo modelo de Comentarios
$nuevoComentario = new Comentarios();


/* CONVIERTE LA FECHA DE FORMATO A INGLES A ESPAÑOL */
setlocale(LC_TIME, 'es_ES.UTF-8');

$this->title = $blog->titulo;


?>

<!-- BANNER -->

<div class="row">
    <div class="col banner-container p-0">
        <h1 class="title title-recursos"><?= Html::encode($this->title) ?></h1>
        <div class="overlay"></div>
        <?= Html::img('@web/img/banner1.webp', ['alt' => 'Banner Img Logo', 'class' => 'img-banner']) ?>
    </div>
</div>

<div class="blog-index blog-view w-100">
    

    <div class="row">
        <div class="col px-0">
            <!-- ACTUALIZAR - BORRAR (ADM)-->
            <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'administrador') : ?>
                <div class="row my-3">
                    <div class="col">
                        <?= Html::a('Actualizar', ['update', 'id' => $blog->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Borrar', ['delete', 'id' => $blog->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => '¿Estás seguro de que deseas eliminar este elemento?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-8">

            <!-- CATEGORIA -->
            <a href="<?= Url::to(['blog/view', 'id' => $blog->id]) ?>" class="btn btn-b btn-category my-4">
                <p class="card-text">
                    <small class="category"><?= Html::encode($blog->categoria) ?></small>
                </p>
            </a>


            <h5 class="card-title">Promoviendo la Educación a través de la Conservación Costera
            </h5>

            <!-- FECHA -->
            <p class="card-text date mb-4">
                <i class="fas fa-calendar-alt"></i> <?= strftime('%e %B, %Y', strtotime($blog->fecha_creacion)) ?>
            </p>

            <!-- Mostrar la imagen -->
            <?php if ($blog->imagen) : ?>
                <?= Html::img(Url::to('@web/' . $blog->imagen), ['class' => 'blog-img card-img-top img-view', 'alt' => 'Imagen']) ?>
            <?php endif; ?>



            <h6 class="pt-4">Introducción</h6>

            <p>
                En un mundo donde la educación es clave para avanzar, la limpieza de playas se convierte en un rayo de esperanza que busca transformar vidas a través del aprendizaje. En este artículo, exploramos cómo la organización [Nombre de la Organización] está cambiando el juego en la educación y cómo está marcando la diferencia en comunidades de todo el mundo.
            </p>

            <h6 class="pt-4">Desafíos en la Protección Costera</h6>

            <p>
                La organización no duda en enfrentar los desafíos. Identifican y abordan problemas críticos en la protección costera y la conservación marina, desde la contaminación por plásticos hasta la degradación del hábitat. Reconocen que aún queda mucho por hacer, pero están comprometidos a seguir adelante y marcar la diferencia.
            </p>

            <hr>

            <!-- Mostrar el conteo de comentarios solo si es mayor a 0 -->
            <?php if ($comentariosCount > 0) : ?>
                <h5 class="title-view-blog">
                    <?= $comentariosCount ?> comentarios
                </h5>
            <?php endif; ?>


            <!-- Mostrar comentarios existentes -->
            <?php
            setlocale(LC_TIME, 'es_ES.UTF-8');
            ?>

            <?php foreach ($comentarios as $comentario) : ?>
                <div class="row mt-4 list-comment pb-3 mb-3" style="border-bottom: 1px solid #ddd;">
                    <div class="col-12 d-flex align-items-start">
                        <!-- Imagen del usuario -->
                        <?= Html::img($comentario->usuario->foto ? Yii::getAlias('@web') . '/' . $comentario->usuario->foto : Yii::getAlias('@web') . '/img/upload-user/photo-default.webp', [
                            'alt' => 'Imagen de perfil',
                            'class' => 'rounded-circle mr-3 img-perfil-blog',
                        ]) ?>

                        <div class="d-flex flex-column w-100">
                            <!-- Nombre del usuario -->
                            <div class="d-flex align-items-center">
                                <strong class="mr-2"><?= Html::encode($comentario->usuario->nombre . ' ' . $comentario->usuario->apellidos) ?></strong>
                                <span class="text-muted ml-auto"><?= strftime('%d de %B de %Y / %H:%M', strtotime($comentario->fecha_comentario)) ?></span>
                            </div>
                            <!-- Comentario -->
                            <div class="mt-1">
                                <?= Html::encode($comentario->comentario) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>






            <!-- Casilla de Comentarios -->
            <?php if (!Yii::$app->user->isGuest) : ?>
                <div class="row mt-4 comment">
                    <div class="col container-c">
                        <h6 class="title-comment-form">Comentarios</h6>
                        <?php $form = ActiveForm::begin(['action' => ['comentarios/create', 'blog_id' => $blog->id]]); ?>
                        <div class="form-group">
                            <?= $form->field($nuevoComentario, 'comentario')->textarea(['rows' => 3, 'class' => 'form-control no-resize', 'placeholder' => 'Escribe tu comentario aquí'])->label(false) ?>
                        </div>
                        <div class="form-group">
                            <?= Html::submitButton('Enviar', ['class' => 'btn btn-clean btn-enviar-view-blog']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-4 c-2">


            <div class="b-2 mt-5">
                <h6 class="title-right">Categorías</h6>
                <ul class="p-0 categorias-donar categorias-blog categoria-view">
                    <?php foreach ($categories as $category) : ?>
                        <li><?= Html::a(Html::encode($category['categoria'] . ' (' . $category['count'] . ')'), ['/ruta/a/' . strtolower($category['categoria'])], ['class' => 'item-b']) ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>


            <!-- Post Recientes -->
            <div class="b-2 mt-5">
                <h6>Blog Recientes</h6>
                <ul class="p-0 categorias-donar categorias-blog blog-reciente">
                    <?php foreach ($ultimosTresBlogs as $blog) : ?>
                        <li class="d-flex align-items-center">
                            <?php if ($blog->imagen) : ?>
                                <div class="img-blog d-flex">
                                    <img src="<?= Yii::getAlias('@web') . $blog->imagen ?>" alt="Imagen del blog" class="img-rec">
                                </div>
                            <?php endif; ?>
                            <h6 class="title-recent-blog text-center"><?= Html::encode($blog->titulo) ?></h6>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <!-- MODAL -->
            <div class="b-3 mt-5">
                <h6>Galería</h6>
                <?= Html::img('@web/img/ocean.webp', ['class' => 'modal-img', 'alt' => 'Imagen 1']) ?>
                <?= Html::img('@web/img/limp-playa.webp', ['class' => 'modal-img', 'alt' => 'Imagen 2']) ?>
                <?= Html::img('@web/img/voluntariadov2.webp', ['class' => 'modal-img', 'alt' => 'Imagen 3']) ?>
                <?= Html::img('@web/img/limpieza-playa.webp', ['class' => 'modal-img', 'alt' => 'Imagen 1']) ?>
                <?= Html::img('@web/img/voluntariado.png', ['class' => 'modal-img', 'alt' => 'Imagen 2']) ?>
                <?= Html::img('@web/img/ocean.webp', ['class' => 'modal-img', 'alt' => 'Imagen 3']) ?>
                <!-- Modal -->
                <div id="myModal" class="modal">

                    <!-- Contenido del modal -->
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <img id="modalImage" src="" alt="">
                    </div>

                </div>
            </div>
        </div>


    </div>



</div>