<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Blog';
?>
<div class="blog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="blog-form">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

        <?php if ($model->isNewRecord) : ?>
            <?= $form->field($model, 'categoria')->dropDownList([
                'voluntariado' => 'Voluntariado',
                'medio ambiente' => 'Medio Ambiente',
                'conservacion' => 'Conservación'
            ], ['prompt' => 'Seleccionar categoría']) ?>
        <?php else : ?>
            <?= $form->field($model, 'categoria')->dropDownList([
                'voluntariado' => 'Voluntariado',
                'medio ambiente' => 'Medio Ambiente',
                'conservacion' => 'Conservación'
            ]) ?>
        <?php endif; ?>

        <?= $form->field($model, 'imagen')->fileInput() ?> <!-- Campo de imagen -->

        <div class="form-group">
            <?= Html::submitButton('Actualizar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
