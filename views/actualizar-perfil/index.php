<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActualizarPerfil */
/* @var $usuario app\models\Usuarios */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Actualizar Perfil';
?>

<div class="container container-update-perfil">
    <div class="row">
        <!-- DEFAULT PERFIL -->
        <div class="col-4 d-flex align-items-center justify-content-center flex-column data-detail">
            <?= Html::img($usuario->foto ? Yii::getAlias('@web') . '/' . $usuario->foto : Yii::getAlias('@web') . '/img/upload-user/photo-default.webp', [
                'alt' => 'Imagen de perfil',
                'class' => 'img-update-perfil mr-3 mt-3',
                'id' => 'profile-picture-preview',
                'style' => 'width: 100px; height: 100px;',
            ]) ?>

            <div class="detail text-center text-white">
                <h2 class="title"><?= Html::encode($usuario->nombre) . ' ' . Html::encode($usuario->apellidos) ?></h2>
                <p class="email mb-0">
                    <i class="fa-solid fa-at"></i>
                    <?= Html::encode($usuario->correo) ?>
                </p>
                <?php if (!empty($usuario->ubicacion)) : ?>
                    <i class="fa-solid fa-location-dot mr-2"></i><?= Html::encode($usuario->ubicacion) ?>
                <?php endif; ?>
                
            </div>
        </div>

        <!-- DETAIL UPDATE PERFIL -->
        <div class="col-8 mt-5 text-center update-perfil-form">
            <h2>Actualizar Perfil</h2>
            <div class="usuario-form">
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>




                <div class="form-group d-flex align-items-center justify-content-center flex-column">
                    <div class="buttons mb-3 position-relative" style="display: inline-block;">
                        <?= Html::img($usuario->foto ? Yii::getAlias('@web') . '/' . $usuario->foto : Yii::getAlias('@web') . '/img/upload-user/photo-default.webp', [
                            'alt' => 'Imagen de perfil',
                            'class' => 'img-update-perfil',
                            'id' => 'profile-picture-preview-update',
                            'style' => 'width: 120px; height: 120px;',
                        ]) ?>
                        <?= $form->field($model, 'imageFile', [
                            'template' => '{input}{label}{error}',
                            'options' => ['tag' => false],
                            'labelOptions' => ['class' => 'position-absolute', 'style' => 'bottom: 0; right: 0; margin: 0;']
                        ])->fileInput([
                            'id' => 'profile-picture-input',
                            'class' => 'inputfile',
                            'style' => 'display: none;',
                        ])->label('<i class="fa-solid fa-camera"></i>') ?>
                    </div>
                </div>
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'value' => $model->nombre]) ?>
                <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true, 'value' => $model->apellidos]) ?>
                <?= $form->field($model, 'ubicacion')->textInput(['maxlength' => true])->label('Ubicación') ?>

                <div class="form-group">
                    <?= Html::submitButton('ACTUALIZAR', ['class' => 'btn btn-success btn btn-clean w-100']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById('profile-picture-input').addEventListener('change', function(event) {
        var preview = document.getElementById('profile-picture-preview');
        var previewUpdate = document.getElementById('profile-picture-preview-update');
        var file = event.target.files[0];
        var reader = new FileReader();

        // Validación del tamaño del archivo en el lado del cliente
        if (file && file.size > 2 * 1024 * 1024) { // 2MB
            alert('El tamaño de la imagen no puede exceder los 2MB.');
            event.target.value = ''; // Limpiar el input
            var defaultSrc = '<?= $usuario->foto ? Yii::getAlias('@web') . '/' . $usuario->foto : Yii::getAlias('@web') . '/img/upload-user/photo-default.webp' ?>';
            preview.src = defaultSrc;
            previewUpdate.src = defaultSrc;
            return;
        }

        reader.onloadend = function() {
            preview.src = reader.result;
            previewUpdate.src = reader.result;
        };

        if (file) {
            reader.readAsDataURL(file);
        } else {
            var defaultSrc = '<?= $usuario->foto ? Yii::getAlias('@web') . '/' . $usuario->foto : Yii::getAlias('@web') . '/img/upload-user/photo-default.webp' ?>';
            preview.src = defaultSrc;
            previewUpdate.src = defaultSrc;
        }
    });

    // Actualiza la imagen de perfil después de actualizar el formulario
    <?php if (Yii::$app->session->hasFlash('success')) : ?>
        var updatedPreview = document.getElementById('profile-picture-preview');
        var updatedPreviewUpdate = document.getElementById('profile-picture-preview-update');
        var updatedSrc = '<?= $usuario->foto ? Yii::getAlias('@web') . '/' . $usuario->foto : Yii::getAlias('@web') . '/img/upload-user/photo-default.webp' ?>';
        updatedPreview.src = updatedSrc;
        updatedPreviewUpdate.src = updatedSrc;
    <?php endif; ?>
</script>

<?php if (Yii::$app->session->hasFlash('error')) : ?>
    <div class="alert alert-danger">
        <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>