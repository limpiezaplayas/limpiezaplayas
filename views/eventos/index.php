<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var app\models\Eventos[] $eventos */

$this->title = 'Eventos';
?>

<div class="row">
    <div class="col banner-container p-0">
        <h1 class="title title-recursos">Próximos eventos</h1>
        <div class="overlay"></div>
        <?= Html::img('@web/img/banner1.webp', ['alt' => 'Banner Img Logo', 'class' => 'img-banner']) ?>
    </div>
</div>

<div class="eventos-index container">
    <div class="row">
        <div class="col">
            <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'administrador') : ?>
                <p>
                    <?= Html::a('Crear Evento', ['create'], ['class' => 'btn btn-clean']) ?>
                </p>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <?php foreach ($eventos as $evento) : ?>
            <div class="col-4 card-c">
                <a href="<?= Url::to(['view', 'id' => $evento->id]) ?>" class="card-link">
                    <div class="card shadow-sm">
                        <div class="img-evento-card">
                            <small class="estado-evento"><?= Html::encode($evento->estado_evento) ?></small>
                            <img src="<?= Url::to('@web/' . $evento->foto) ?>" class="bd-placeholder-img card-img-top" width="100%" height="225" alt="Imagen del evento" class="img-playa">

                            <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'administrador') : ?>
                                <div class="update-delete">
                                <!-- BTN ACTUALZIAR -->    
                                <a href="<?= Url::to(['update', 'id' => $evento->id]) ?>" class="btn btn-sm btn-outline-secondary bg-dark"><i class="fa-regular fa-pen-to-square text-success"></i></a>
                                    
                                    <!-- BTN ELIMINAR -->
                                    <a href="<?= Url::to(['delete', 'id' => $evento->id]) ?>" class="btn btn-sm btn-outline-secondary bg-dark" data-method="post" data-confirm="¿Estás seguro de eliminar este evento?"><i class="fa-regular fa-trash-can text-danger"></i></a>
                                </div>
                            <?php endif; ?>

                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><?= Html::encode($evento->playas->nombre) ?></h5>
                            <div class="ubicacion d-flex align-items-center mb-2">
                                <i class="fa-solid fa-location-dot text-muted"></i>&nbsp;
                                <h6 class="card-subtitle mt-0 text-muted"><?= Html::encode($evento->playas->ubicacion) ?></h6>
                            </div>
                            <p class="card-text">
                                <i class="fa-regular fa-calendar"></i>
                                <small class="fecha-evento"><?= Html::encode(date('d-m-Y', strtotime($evento->fecha_evento))) ?></small>&nbsp;

                                <i class="fa-regular fa-clock"></i>
                                <small class="fecha-evento"><?= Html::encode((new \DateTime($evento->hora_evento))->format('H:i')) ?></small>
                            </p>
                            <p class="card-text mb-3"><?= Html::encode($evento->descripcion) ?></p>

                            <div class="d-flex align-items-center">
                                <div class="btn-group w-100 btn-view-card">
                                    <a href="<?= Url::to(['view', 'id' => $evento->id]) ?>" class="btn btn-clean w-100 btn-view">Ver</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>

