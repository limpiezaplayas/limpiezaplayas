<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Eventos $model */
/** @var app\models\Playas $playa */

\yii\web\YiiAsset::register($this);
?>
<?php

use yii\helpers\Url;




/* CONVIERTE LA FECHA DE FORMATO A INGLES A ESPAÑOL */

setlocale(LC_TIME, 'es_ES.UTF-8');

$this->title = "Eventos";


?>

<!-- BANNER -->
<div class="row">
    <div class="col banner-container p-0">
        <h1 class="title title-recursos"><?= Html::encode($this->title) ?></h1>
        <div class="overlay"></div>
        <?= Html::img('@web/img/banner1.webp', ['alt' => 'Banner Img Logo', 'class' => 'img-banner']) ?>
    </div>
</div>

<div class="blog-index blog-view w-100">
    <div class="row">
        <div class="col px-0">
            <!-- ACTUALIZAR - BORRAR (ADM)-->
            <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'administrador') : ?>
                <div class="row my-3">
                    <div class="col">
                        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => '¿Estás seguro de que deseas eliminar este elemento?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-8">

            <!-- CATEGORIA -->
            <a href="<?= Url::to(['blog/view', 'id' => $model->id]) ?>" class="btn btn-b btn-category my-4">
                <p class="card-text">
                    <small class="category"><?= Html::encode($model->estado_evento) ?></small>
                </p>
            </a>


            <h5 class="card-title"><?= Html::encode($playa->nombre) ?></h5>

            <!-- Ubicación -->
            <div class="ubicacion d-flex align-items-center mb-2">
                <i class="fa-solid fa-location-dot text-muted" style="font-size:24px;"></i>&nbsp;
                <?php if ($playa !== null) : ?>
                    <h6 class="card-subtitle mt-0 text-muted"><?= Html::encode($playa->ubicacion) ?></h6>
                <?php endif; ?>
            </div>

            <!-- FECHA y hora -->
            <p class="card-text date mb-4">
                Evento programado a
                <?= strftime('%e %B, %Y', strtotime($model->fecha_evento_creado)) ?>&nbsp;&nbsp;
                <i class="fa-regular fa-clock" style="font-size: 20px;"></i>
                <span class="fecha-evento"><?= Html::encode((new \DateTime($model->hora_evento))->format('H:i')) ?></span>
            </p>

            <!-- Mostrar la imagen -->
            <?php if ($model->foto) : ?>
                <?= Html::img(Url::to('@web/' . $model->foto), ['class' => 'blog-img card-img-top img-view', 'alt' => 'Imagen']) ?>
            <?php endif; ?>



            <h6 class="pt-4">Información</h6>

            <p class="mb-5">
                <?= Html::encode($model->descripcion) ?>
            </p>


        </div>

        <div class="col-4 c-2">


            <!-- Últimos Eventos Recientes -->
            <!-- Últimos Eventos Recientes -->
            <div class="b-2 mt-5">
                <h6>Próximos eventos</h6>
                <ul class="p-0 categorias-donar categorias-blog blog-reciente evento-reciente">
                    <?php foreach ($ultimosEventos as $evento) : ?>
                        <li class="d-flex align-items-center">
                            <!-- Mostrar la imagen del evento -->
                            <?php if ($evento->foto) : ?>
                                <?= Html::img(Url::to('@web/' . $evento->foto), ['class' => 'img-evento', 'alt' => 'Imagen del evento']) ?>
                            <?php endif; ?>
                            <h6 class="title-recent-blog text-center d-flex flex-column">
                                <?= Html::encode($evento->playas->nombre) ?>
                                <span class="date-event-view">
                                    <?= strftime('%e %B, %Y', strtotime($evento->fecha_evento)) ?>&nbsp;&nbsp;
                                    <i class="fa-regular fa-clock" style="font-size: 12px;"></i>
                                    <span class="fecha-evento"><?= Html::encode((new \DateTime($evento->hora_evento))->format('H:i')) ?></span>
                                </span>
                            </h6>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>




            <!-- MODAL -->
            <div class="b-3 mt-5">
                <h6>Galería</h6>
                <?= Html::img('@web/img/ocean.webp', ['class' => 'modal-img', 'alt' => 'Imagen 1']) ?>
                <?= Html::img('@web/img/limp-playa.webp', ['class' => 'modal-img', 'alt' => 'Imagen 2']) ?>
                <?= Html::img('@web/img/voluntariadov2.webp', ['class' => 'modal-img', 'alt' => 'Imagen 3']) ?>
                <?= Html::img('@web/img/limpieza-playa.webp', ['class' => 'modal-img', 'alt' => 'Imagen 1']) ?>
                <?= Html::img('@web/img/voluntariado.png', ['class' => 'modal-img', 'alt' => 'Imagen 2']) ?>
                <?= Html::img('@web/img/ocean.webp', ['class' => 'modal-img', 'alt' => 'Imagen 3']) ?>
                <!-- Modal -->
                <div id="myModal" class="modal">

                    <!-- Contenido del modal -->
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <img id="modalImage" src="" alt="">
                    </div>

                </div>
            </div>
        </div>


    </div>
</div>