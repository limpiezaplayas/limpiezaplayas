<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Eventos $model */

?>
<div class="eventos-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
