<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Playas;

/* @var $this yii\web\View */
/* @var $model app\models\Eventos */
/* @var $form yii\widgets\ActiveForm */

$playas = Playas::find()->select(['id', 'nombre', 'ubicacion'])->all();
$playasList = ArrayHelper::map($playas, 'id', 'nombre');
?>

<div class="eventos-form container p-5">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <!-- Campo para mostrar la vista previa de la imagen -->
            <div class="profile-picture-container event-picture-container text-center">
                <h1 class="title">Crear evento</h1>
                <div class="profile-picture event-picture">
                    <?= Html::img($model->foto ? '@web/' . $model->foto : '@web/img/upload-event/event-default.jpg', [
                        'alt' => 'Imagen del evento',
                        'class' => 'img-update-event',
                        'id' => 'event-picture-preview',
                    ]) ?>
                    <div class="upload-button">
                        <?= Html::label('<i class="fas fa-camera"></i>', 'event-picture-input', ['class' => 'btn btn-upload']) ?>
                        <?= $form->field($model, 'imageFile')->fileInput([
                            'id' => 'event-picture-input',
                            'class' => 'd-none',
                        ])->label(false) ?>
                    </div>
                </div>
            </div>

            <!-- Playas Desplegable -->
            <?= $form->field($model, 'playas_id')->dropDownList($playasList, ['prompt' => 'Seleccione una playa', 'id' => 'playas-dropdown'])->label('Playas') ?>

            <!-- Ubicación -->
            <div class="form-group">
                <?= Html::label('Ubicación', 'ubicacion-field', ['class' => 'control-label']) ?>
                <?= Html::textInput('ubicacion', $model->playas ? $model->playas->ubicacion : '', ['class' => 'form-control', 'readonly' => true, 'id' => 'ubicacion-field']) ?>
            </div>

            <!-- Seleccionar Fecha -->
            <?= $form->field($model, 'fecha_evento')->widget(DatePicker::classname(), [
                'dateFormat' => 'dd-MM-yyyy',
                'options' => ['class' => 'form-control', 'readonly' => true],
                'clientOptions' => [
                    'minDate' => 0, // Permite seleccionar desde hoy
                    'maxDate' => '+2m', // Permite seleccionar 2 meses
                    'changeMonth' => true, // Permite la selección directa del mes
                    'changeYear' => false, // No permite cambiar de año
                    'yearRange' => '0:+0', // Rango de años permitidos
                    'theme' => 'base',
                    'placeholder' => 'Seleccione fecha',
                    'autocomplete' => 'off',
                ],
            ])->label('Fecha') ?>

            <!-- Seleccionar Hora -->
            <?= $form->field($model, 'hora_evento')->input('time', ['class' => 'form-control'])->label('Hora') ?>

            <div class="form-group">
                <label for="descripcion">Descripción</label>
                <small id="char-count" class="form-text text-muted">0/500 caracteres</small>
                <?= $form->field($model, 'descripcion')->textarea([
                    'rows' => 6,
                    'id' => 'descripcion',
                    'maxlength' => true,
                ])->label(false) ?>
            </div>


            <!-- Estado del Evento -->
            <?= $form->field($model, 'estado_evento')->dropDownList([
                'planificado' => 'Planificado',
                'en curso' => 'En Curso',
                'concluido' => 'Concluido',
            ])->label('Estado del Evento') ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => 'btn btn-clean w-100']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>


</div>

<script>
    // Asignar ubicaciones de playas a un objeto para fácil acceso
    var playas = <?= json_encode(ArrayHelper::map($playas, 'id', 'ubicacion')) ?>;

    document.getElementById('event-picture-input').addEventListener('change', function(event) {
        var preview = document.getElementById('event-picture-preview');
        var file = event.target.files[0];
        var reader = new FileReader();

        reader.onloadend = function() {
            preview.src = reader.result;
        };

        if (file) {
            reader.readAsDataURL(file);
        }
    });

    // Actualizar campo de ubicación cuando se seleccione una playa
    document.getElementById('playas-dropdown').addEventListener('change', function() {
        var playaId = this.value;
        var ubicacionField = document.getElementById('ubicacion-field');
        if (playas[playaId]) {
            ubicacionField.value = playas[playaId];
        } else {
            ubicacionField.value = '';
        }
    });


    document.addEventListener('DOMContentLoaded', function() {
    var textarea = document.getElementById('descripcion');
    var charCount = document.getElementById('char-count');

    textarea.addEventListener('input', function() {
        var textLength = textarea.value.length;
        charCount.textContent = textLength + '/500 caracteres';
    });
});

</script>