<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\RecursosEducativos $model */

$this->title = 'Create Recursos Educativos';
?>
<div class="recursos-educativos-create m-4">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
