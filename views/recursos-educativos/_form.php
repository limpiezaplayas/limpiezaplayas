<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\RecursosEducativos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="row row-form-component m-4">
    <div class="col ">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'titulo')->textInput(['maxlength' => true, 'id' => 'titulo-input'])->label('Título') ?>

        <?= $form->field($model, 'descripcion')->textarea(['rows' => 6, 'maxlength' => true, 'id' => 'descripcion-textarea'])->label('Descripción') ?>
        <div id="char-count">0/500</div>

        <?= $form->field($model, 'enlace_recurso')->textInput(['maxlength' => true, 'type' => 'url'])->label('Enlace Recurso') ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => 'btn btn-clean w-100']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<script>
document.addEventListener('DOMContentLoaded', (event) => {
    const descripcionTextarea = document.getElementById('descripcion-textarea');
    const charCount = document.getElementById('char-count');

    descripcionTextarea.addEventListener('input', () => {
        const length = descripcionTextarea.value.length;
        charCount.textContent = `${length}/500`;
    });

    // Initialize the character count on page load
    charCount.textContent = `${descripcionTextarea.value.length}/500`;
});
</script>
