<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\models\RecursosEducativos */

$this->title = 'Recursos Educativos';
?>
<div class="row">
    <div class="col banner-container p-0">
        <h1 class="title title-recursos"><?= Html::encode($this->title) ?></h1>
        <div class="overlay"></div>
        <?= Html::img('@web/img/banner1.webp', ['alt' => 'Banner Img Logo', 'class' => 'img-banner']) ?>
    </div>
</div>

<div class="recursos-educativos-index w-100 flex-column align-items-center mt-5">

    <div class="row d-flex justify-content-between align-items-center" style="padding-right: 15px; padding-left: 15px;">
        <div class="search-bar">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
            <?= $form->field($searchModel, 'titulo')->textInput(['id' => 'search-input', 'placeholder' => 'Buscar por título...'])->label(false) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="action-buttons my-4" style="padding-left: 15px; padding-right: 15px;">
        <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'administrador') : ?>
            <?= Html::a('Crear Recurso', ['create'], ['class' => 'btn btn-clean']) ?>
        <?php endif; ?>
    </div>

    <div id="no-results" class="alert alert-warning" style="display:none;">
        No existen recursos disponibles con esa palabra.
    </div>

    <div class="row" id="resource-cards">
        <?php if ($dataProvider->getCount() > 0) : ?>
            <?php foreach ($dataProvider->models as $model) : ?>
                <div class="col-md-4 resource-item">
                    <div class="card mb-4 shadow-sm">
                        <div class="card-body">
                            <h5 class="card-title"><?= Html::encode($model->titulo) ?></h5>
                            <p class="card-text"><?= Html::encode($model->descripcion) ?></p>
                            <div class="d-flex justify-content-between align-items-center buttons-footer-recursos">
                                <div class="btn-group">
                                    <?= Html::a('Ir al recurso', $model->enlace_recurso, ['class' => 'btn btn-clean', 'target' => '_blank']) ?>
                                </div>
                                <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'administrador') : ?>
                                    <div class="update-delete">
                                        <?= Html::a('<i class="fa-regular fa-pen-to-square text-success"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-outline-secondary bg-dark']) ?>
                                        <?= Html::a('<i class="fa-regular fa-trash-can text-danger"></i>', ['delete', 'id' => $model->id], [
                                            'class' => 'btn btn-sm btn-outline-secondary bg-dark',
                                            'data' => [
                                                'confirm' => '¿Estás seguro de eliminar este recurso?',
                                                'method' => 'post',
                                            ],
                                        ]) ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <div class="col-12">
                <div id="no-results-server" class="alert alert-warning">
                    No existen recursos disponibles.
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="pagination-wrapper">
        <?= LinkPager::widget([
            'pagination' => $dataProvider->pagination,
        ]) ?>
    </div>
</div>

<script>
    document.getElementById('search-input').addEventListener('keyup', function() {
        var input, filter, cards, cardContainer, title, i, visible;
        input = document.getElementById('search-input');
        filter = input.value.toUpperCase();
        cardContainer = document.getElementById('resource-cards');
        cards = cardContainer.getElementsByClassName('resource-item');
        visible = false;
        for (i = 0; i < cards.length; i++) {
            title = cards[i].querySelector(".card-title");
            if (title.innerText.toUpperCase().indexOf(filter) > -1) {
                cards[i].style.display = "";
                visible = true;
            } else {
                cards[i].style.display = "none";
            }
        }
        document.getElementById('no-results').style.display = visible ? 'none' : 'block';
    });

    document.getElementById('search-input').addEventListener('keypress', function(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            var input, filter, cards, cardContainer, title, i, visible;
            input = document.getElementById('search-input');
            filter = input.value.toUpperCase();
            cardContainer = document.getElementById('resource-cards');
            cards = cardContainer.getElementsByClassName('resource-item');
            visible = false;
            for (i = 0; i < cards.length; i++) {
                title = cards[i].querySelector(".card-title");
                if (title.innerText.toUpperCase().indexOf(filter) > -1) {
                    cards[i].style.display = "";
                    visible = true;
                } else {
                    cards[i].style.display = "none";
                }
            }
            document.getElementById('no-results').style.display = visible ? 'none' : 'block';
        }
    });
</script>