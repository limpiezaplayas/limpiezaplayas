<?php

use app\models\RecursosEducativos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Recursos Educativos';
?>
<div class="recursos-educativos-index">

    <h1 class="tabla-h1"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Recursos Educativos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-dark table-hover'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'titulo',
            'descripcion',
            'enlace_recurso',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, RecursosEducativos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>