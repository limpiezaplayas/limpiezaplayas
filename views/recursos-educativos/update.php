<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\RecursosEducativos $model */

$this->title = 'Actualizar Recurso';
?>
<div class="recursos-educativos-update container mt-5 d-flex flex-column">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <?= Yii::$app->session->getFlash('success') ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>

    <div class="recursos-educativos-form d-flex justify-content-center">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>

</div>
