<?php

use yii\web\View;
use yii\helpers\Html;

/** @var yii\web\View $this */
?>


<div class="home sec-1">
    <!-- VIDE HOME -->
    <div class="video-container">
        <video class="videoBG" autoplay muted loop>
            <source src="<?= Yii::getAlias('@web/videos/limpiezaBG.mp4') ?>" type="video/mp4">
            Tu navegador no admite el elemento <code>video</code>.
        </video>
        <div class="video-overlay"></div>

    </div>

    <!-- REDES -->
    <div class="redes">
        <div class="line"></div>
        <ul class="mb-0">
            <li class="link-item">
                <?= Html::a(
                    '<i class="fa-brands fa-gitlab"></i>',
                    'https://gitlab.com/limpiezaplayas/limpiezaplayas',
                    [
                        'target' => '_blank',
                        'class' => 'github-icon'
                    ]
                ) ?>
            </li>
            <li class="link-item">
                <?= Html::a(
                    '<i class="fab fa-linkedin"></i>',
                    'https://www.linkedin.com/',
                    [
                        'target' => '_blank',
                        'class' => 'linkedin-icon'
                    ]
                ) ?>
            </li>
            <li class="link-item">
                <?= Html::a(
                    '<i class="fab fa-github"></i>',
                    'https://github.com/christopherqmendivel?tab=repositories',
                    [
                        'target' => '_blank',
                        'class' => 'github-icon'
                    ]
                ) ?>
            </li>
        </ul>

        <div class="line"></div>

    </div>

    <!-- INFO HOMEPAGE -->
    <div class="layout">
        <div class="text">
            <h1 class="title text-center">Únete a<br> Nuestra Misión de Playas Limpias</h1>
            <p class="desc">
                Únete a nuestra misión de mantener las playas limpias. Haz la diferencia y preserva la belleza natural de nuestros océanos. Únete a nuestra comunidad comprometida con la limpieza costera.
            </p>
            <?= Html::a('Únete ahora', ['registro-usuarios/create'], ['class' => 'btn btn-clean']) ?>

        </div>
    </div>
</div>


<div class="row sec-2 d-flex w-100 align-items-center">
    <div class="left col-5">
        <?= Html::img(
            '@web/img/olas.png',
            [
                'alt' => 'Playa - Olas',
                'class' => 'img-fluid icon-sec'
            ]
        );
        ?>
        <div class="pre-title">
            <img src="<?= Yii::$app->request->baseUrl ?>/img/line.svg" class="line" alt="line SVG">
            <span class="title">Quiénes somos</span>
        </div>
        <h2 class="title">Misión</h2>
        <p class="info desc">
            Bienvenido a EcoPlayas, una iniciativa comprometida con la preservación del medio ambiente y la limpieza de nuestras playas. Creemos firmemente en la importancia de cuidar y proteger nuestros ecosistemas costeros, y estamos dedicados a hacer nuestra parte para asegurar un futuro más limpio y sostenible para las generaciones venideras.
        </p>


        <?= Html::a('Quiénes somos', ['/quienes-somos/index'], ['class' => 'btn btn-clean']) ?>


    </div>


    <div class="right col-6 p-0">
        <?= Html::img(
            '@web/img/limpieza-playa.webp',
            [
                'alt' => 'Limpieza Playa',
                'class' => 'img-fluid rounded'
            ]
        );
        ?>

    </div>
</div>

<div class="row sec-3">
    <!-- IMAGE -->
    <div class="col-6 p-0 image-container">
        <?= Html::img(
            '@web/img/limp-playa.webp',
            [
                'alt' => 'Limpiando en la playa',
                'class' => 'img-fluid w-100 sec-3_img'
            ]
        );
        ?>
    </div>

    <!-- INFO -->
    <div class="col-6 container-info">
        <svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 288.8 281.1" class="icon-colaborar">
            <path id="Forma_33" class="st0" d="M0,216.8c4.4-5.4,8.5-11,13.2-16.1c8.9-9.8,12.5-20.6,8.3-33.7c-1.1-3.5-2.2-6.6-4.7-9.5 c-9.2-11-9.5-26.7,4-35.2c0.5-0.4,1-0.8,1.5-1.2c-4.3-9.5-5-18.7,2.2-27.3c-5.6-5.7-8.4-12.5-7.6-20.8c0.7-9.6,7.7-17.5,17.1-19.5 c1.7-0.4,1.7-1.3,1.7-2.6c-0.1-8.3,2.8-15.1,9.9-19.7c6-3.9,12.5-4,19.2-2.3c1,0.3,2,0.6,3.3,1c0.7-2.1,1.5-4.2,2.4-6.2 c2.2-4.6,4.5-9.3,7.1-13.7c1.3-2,3-3.8,5-5.3c9.8-7.9,20.2-5.9,26.7,5.1c3.8-4.5,8.4-8.1,14.2-9c4-0.7,8.2-0.8,12.2-0.2 c7.5,1.4,12.1,7.3,12.7,14.9c0.1,1.3,0.2,2.6,0.3,4.4c1.1-0.8,1.8-1.4,2.6-1.9c6-4.2,12.4-7.1,20-5.3c8,2,13.8,8.9,14.3,17.2 c0.4,5.3-0.4,10.6-2.4,15.5c-4.9,11.2-10,22.2-15.1,33.3c-1.1,2.3-2.8,3.8-5.6,3.4c-2.7-0.4-4.9-1.9-5.3-4.7c-0.1-1.7,0.3-3.5,1.1-5 c4.6-9.4,9.4-18.6,13.9-28c2.1-4.2,3.6-8.7,3.2-13.5c-0.4-6.6-5.8-10.5-12.2-8.8c-5.5,1.4-10.2,5.1-12.9,10.2 C145.2,42,140,51.7,135,61.5c-1.2,2.8-4.4,4.1-7.2,2.9c-1-0.4-1.8-1.1-2.4-2c-1.5-2.1-0.8-4.2,0.1-6.4c3.3-8.1,6.8-16.1,9.4-24.4 c1.3-4,1.1-8.5,1-12.8c-0.1-3.9-2.5-6.6-6.4-7.8c-4.2-1.3-7.9-0.7-10.6,2.9c-2,2.7-3.8,5.6-5.2,8.6c-3.8,8.5-7.4,17-11.2,25.7 c5.2,4.5,8.4,10,8.8,16.8c0.5,6.8-2.3,12.4-7.2,17.3c6.9,6.2,10.9,13.8,9.4,23.2c-1.4,9.3-6.1,16.2-15.4,19.5 c3.1,13.1-2.8,21.6-14,27.2c0.8,2.6,1.5,5.3,2.1,8c2.1,14.6-14,27.5-27.9,21.6c-7.9-3.4-15.5-7.5-23.3-11.2 c-0.9-0.4-1.8-0.9-2.9-1.6c0.4,2.4,0.8,4.4,1.2,6.4c1.4,6.8,0.1,13.9-3.4,19.9c-3.7,6.3-8,12.3-12.1,18.3c-1.7,2.5-3.8,4.7-5.4,7.2 c-3.3,5.6-9.3,5.3-12.5-0.2L0,216.8L0,216.8z M47.1,53.5c0.1,0,0.1,0,0.2,0c0,2.3,0.1,4.5,0,6.8c-0.1,3-2.5,5.3-5.5,5.3 c-1,0-2.1-0.1-3.1-0.3c-6.3-1.1-11.3,6.2-9.8,11.9c1,3.9,3.4,7.2,6.9,9.3c5.9,3.6,11.9,7.1,18.1,10.3c11.8,6.1,24,11.5,36.9,15.1 c5.6,1.6,9.7-1.5,9.5-7.3c-0.1-2.5-0.9-4.9-2.3-7c-2.8-4.3-7.4-6.6-12-8.6c-7.5-3.2-15-6.5-22.5-9.7c-2.6-1.1-5.2-2.2-7.7-3.5 c-3.9-1.9-5.4-6.6-3.5-10.5c0-0.1,0.1-0.1,0.1-0.2c1.7-3.5,4.6-4.2,9.1-2.3c8.7,3.8,17.4,7.5,26,11.5c2.9,1.6,6.4,1.1,8.9-1 c2.9-2.3,3.8-5.4,3.1-9.1c-0.9-4.4-3.7-7.1-7.4-9.1c-7.3-4-14.7-8-22.2-11.6c-4.1-2-8.5-3.5-12.9-4.7c-4.8-1.3-8.4,1.2-9.4,6 C47.3,47.9,47.1,50.7,47.1,53.5z M32.6,100.8c-3.3,3-4.2,6.4-2.5,10.9c1.8,4.7,5.1,8.7,9.4,11.3c9.1,5.7,18.4,11.1,27.9,16.4 c3.9,2.2,8.4,3,12.8,2.4c4.1-0.6,7.4-4.2,8.1-8.9c0.5-3.9-1.6-7.6-5.2-9.1c-2.1-1-4.3-1.8-6.5-2.8C62.1,114.2,47.4,107.6,32.6,100.8 L32.6,100.8z M65.6,170c2.5,0,4.8-1.3,6.2-3.4c3.1-4.7,1.8-11.1-2.9-14.2c-0.1-0.1-0.2-0.1-0.2-0.2c-8.2-4.9-16.4-9.8-24.7-14.6 c-3.6-2.2-7.3-4.2-11.1-6c-6-2.7-11.6,1.3-11.6,7.8c0,2.9,1.3,5.6,3.4,7.5c8.2,8.1,17.9,13.9,28.3,18.6 C57.1,167.3,61.4,168.5,65.6,170L65.6,170z M93.6,42.2c1.8-5.2,3.9-10.1,5.1-15.2c0.9-3.6,1-7.4,0.4-11.1c-1-5.3-6.6-6.8-10.6-3.1 c-1.7,1.5-3.1,3.3-4.2,5.3c-2.8,5-5.2,10.2-7.9,15.3C82.2,36.3,87.6,39.1,93.6,42.2L93.6,42.2z M261.6,139.7 c-16.1,17.1-32,34.8-48.8,51.6c-17.4,17.5-37.9,30.5-62,36.9c-0.6,0.3-1.2,0.6-1.8,0.9c8.4,3.2,16.1,6.1,23.9,9.2 c11,4.3,22.1,8.2,32.9,13c9,4.1,16.9,10.1,23.1,17.7c0.7,0.8,1.3,1.6,1.9,2.5c1.6,2.7,1,6.2-1.4,8.2c-2.5,2.1-6.2,1.8-8.5-0.5 c-1.3-1.3-2.4-2.9-3.6-4.3c-8.7-10.1-20.2-15.7-32.3-20.3c-10.7-4-21.6-7.6-32.1-12c-5.3-2.2-10.1-5.7-15.2-8.5 c-1.1-0.6-2.3-0.7-3.5-0.4c-7.3,3.1-12.6,8.6-17,14.8c-5.3,7.6-10.1,15.6-15.2,23.4c-1.2,1.8-2.4,3.6-3.6,5.4 c-1.1,1.6-2.4,3.1-4.6,2c-2.2-1-2.5-3-1.7-5c2.7-6.7,5-13.6,8.4-19.9c6.3-11.5,14.2-22,25.9-28.5c6.8-3.7,14.1-6.5,21.6-8.4 c21.5-5.7,39.8-16.8,55.6-32c10.6-10.1,20.1-21.3,30.2-32c3.8-4,7.7-8,11.6-11.9c5.1-5.2,9.4-10.8,10.3-18.3 c0.9-6.7-3.5-10.8-10.1-9.1c-7.6,1.9-13.5,6.9-19.1,11.9c-4.9,4.4-9.6,9.1-14.7,13.2c-3.5,2.9-7.3,5.3-11.3,7.2 c-10.2,4.8-16.2-2.5-16-10.4c0.3-6.6,1.3-13.2,2.9-19.7c2.9-12.8,6.3-25.5,9.5-38.2c0.6-2.3,0.3-4.7-0.8-6.8c-4-8-7.9-16.1-9.2-25 c-0.6-3.5-0.6-7.1-0.1-10.7c1.7-9.9,10.6-15.4,20.2-12c4.8,1.8,9.3,4.4,13.2,7.8c21.1,17.4,34.8,39.9,44.1,65.4 c5.8,16,9.2,32.5,11.9,49.2c2.4,14.8,5.4,29.4,11.6,43.1c2.4,5.3-0.9,10.9-6.6,11.9c-2.9,0.5-5.6-1-6.6-4.4 c-2.8-9.1-5.7-18.3-7.8-27.6C264.7,159.4,263.3,149.2,261.6,139.7L261.6,139.7z M253.8,103.9c0-0.3,0-0.6,0-1 C246,81,234.9,61.1,218,45c-3.9-3.7-8.2-6.8-12.9-9.5c-4.3-2.4-7.3,0-7.2,4.9c0,1.4,0.1,2.8,0.5,4.2c2.1,6.3,4.2,12.6,6.7,18.8 c3.2,7.8,7,15.4,10.3,23.1c2.4,5.5,3.4,11.6,2.9,17.6c-0.3,3.3-1.9,5.2-4.4,5.3c-2.9,0.2-4.4-1.3-5.1-4.8c-0.7-3.4-1.5-6.8-2.3-10.2 l-0.9-0.1c-2.9,10.6-5.9,21.1-8.6,31.7c-0.5,2.4-0.7,4.8-0.6,7.3c0,2,1.2,2.5,2.8,1.5c1.5-1,2.9-2.1,4.2-3.3 c6.3-5.7,12.3-11.7,18.8-17C231.2,107.1,241,101,253.8,103.9L253.8,103.9z"></path>
        </svg>
        <div class="pre-title">
            <img src="<?= Yii::$app->request->baseUrl ?>/img/line-white.svg" class="line" alt="line SVG">
            <span class="title text-white">Colabora</span>
        </div>
        <h2 class="title text-white">¿Cómo colaborar?</h2>

        <p class="desc text-white">
            Puedes participar en nuestros eventos de limpieza, hacer donaciones para apoyar nuestras actividades o difundir nuestra causa en tus redes sociales. ¡Cada acción cuenta para un mar más limpio y saludable!
        </p>

        <?= Html::a(
            'COLABORAR',
            ['/colabora/index'],
            ['class' => 'btn btn-clean']
        )

        ?>

    </div>
</div>

<div class="row sec-4 w-100">
    <h2 class="text-center w-100 colaboradores-slider">Colaboradores</h2>
    <div class="col-12 company logos">
        <div class="d-flex logos-slide">
            <div class="logo-container">
                <img src="<?= Yii::getAlias('@web') ?>/img/logos/logo-ceinmark.png" alt="Logo Ceinmark" class="logo img-fluid">
            </div>

            <div class="logo-container">
                <img src="<?= Yii::getAlias('@web') ?>/img/logos/logo-cic.png" alt="Logo CIC" class="logo img-fluid">
            </div>

            <div class="logo-container">
                <img src="<?= Yii::getAlias('@web') ?>/img/logos/logo-marsh.png" alt="Logo Marsh Rehder" class="logo img-fluid">
            </div>

            <div class="logo-container">
                <img src="<?= Yii::getAlias('@web') ?>/img/logos/logo-rimac.png" alt="Logo Rimac Seguros" class="logo img-fluid">
            </div>
            <div class="logo-container">
                <img src="<?= Yii::getAlias('@web') ?>/img/logos/axpe-logo.png" alt="Logo Axpe Consulting" class="logo img-fluid">
            </div>

            <div class="logo-container">
                <img src="<?= Yii::getAlias('@web') ?>/img/logos/logo-tj.png" alt="Logo TJ MAX" class="logo img-fluid">
            </div>
        </div>
    </div>
</div>