<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Panel de Administrador';
$this->registerJs('
    $(document).ready(function() {
        // Selector de botones de eventos y usuarios
        var eventosButton = $("#eventos-button");
        var usuariosButton = $("#usuarios-button");
        var recursosButton = $("#recursos-button");
        var donacionesButton = $("#donaciones-button");

        // Selector de contenedor de contenido
        var contentContainer = $("#content-container");

        // Función para cargar el contenido de la vista de eventos
        function loadEventos(event) {
            event.preventDefault(); // Prevenir el comportamiento predeterminado del enlace
            $.ajax({
                url: "' . \yii\helpers\Url::to(['eventos']) . '",
                type: "GET",
                success: function(response) {
                    contentContainer.html(response);
                },
                error: function(xhr, status, error) {
                    console.error("Error al cargar la vista de eventos:", error);
                }
            });
        }

        // Función para cargar el contenido de la vista de usuarios
        function loadUsuarios(event) {
            event.preventDefault(); 
            $.ajax({
                url: "' . \yii\helpers\Url::to(['usuarios']) . '",
                type: "GET",
                success: function(response) {
                    contentContainer.html(response);
                    
                },
                error: function(xhr, status, error) {
                    console.error("Error al cargar la vista de usuarios:", error);
                }
            });
        }

        
        // Función para cargar el contenido de la vista de usuarios
        function loadRecursos(event) {
            event.preventDefault(); 
            $.ajax({
                url: "' . \yii\helpers\Url::to(['recursos']) . '",
                type: "GET",
                success: function(response) {
                    contentContainer.html(response);
                    
                },
                error: function(xhr, status, error) {
                    console.error("Error al cargar la vista de usuarios:", error);
                }
            });
        }

        function loadDonaciones(event) {
            event.preventDefault(); 
            $.ajax({
                url: "' . \yii\helpers\Url::to(['donaciones']) . '",
                type: "GET",
                success: function(response) {
                    contentContainer.html(response);
                    
                },
                error: function(xhr, status, error) {
                    console.error("Error al cargar la vista de usuarios:", error);
                }
            });
        }

        // Agrega eventos de clic a los botones
        eventosButton.click(loadEventos);
        usuariosButton.click(loadUsuarios);
        recursosButton.click(loadRecursos);
        donacionesButton.click(loadDonaciones);

        // Verificar si la URL actual es diferente de la deseada y forzar la navegación
        var desiredURL = "http://localhost/limpiezaplayas/web/admin/index";
        if (window.location.href !== desiredURL) {
            window.history.pushState({}, "", desiredURL);
        }
    });
');
?>

<div class="admin-dashboard">
    <div class="row">
        <div class="col-md-3 menu-lateral">
            <h1 class="title"><?= Html::encode($this->title) ?></h1>
            <div class="links">
                <?= Html::a('Usuarios', ['usuarios'], ['class' => 'btn btn-primary', 'id' => 'usuarios-button']) ?>
                <?= Html::a('Eventos', ['eventos'], ['class' => 'btn btn-primary', 'id' => 'eventos-button']) ?>

                <?= Html::a('Recursos Educativos', ['recursos'], ['class' => 'btn btn-primary', 'id' => 'recursos-button']) ?>
                <?= Html::a('Donaciones', ['donaciones'], ['class' => 'btn btn-primary', 'id' => 'donaciones-button']) ?>
            </div>
        </div>
        <div class="col-md-9" id="content-container">
        <h1 class="tabla-h1">Eventos</h1>

            <div class="admin-table">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-dark table-hover'],
                    'columns' => [
                        'fecha_evento_creado',
                        'fecha_evento',
                        'estado_evento',
                        'lugar',
                        'descripcion'

                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>