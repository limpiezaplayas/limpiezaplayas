<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Playas $model */

$this->title = 'Create Playas';
$this->params['breadcrumbs'][] = ['label' => 'Playas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="playas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
