<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\RegistroUsuarios $model */

\yii\web\YiiAsset::register($this);
?>
<div class="registro-usuarios-view mt-3">


    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'ubicacion',
                'value' => function($model) {
                    return $model->ubicacion ? $model->ubicacion : 'No especificado';
                },
            ],
            'nombre',
            'apellidos',
            'password',
            'correo',
            'rol',
        ],
    ]) ?>

</div>
