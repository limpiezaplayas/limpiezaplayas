<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RegistroUsuarios */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Registrar';
?>

<div class="container-principal crear-usuario w-100">
    <div class="container-create-user w-100 h-100">
        <div class="row h-100 s-1 justify-content-center align-items-center animate__animated animate__fadeIn">
            <div class="form-register">
                <h1 class="title">Registrar</h1>

                <?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

                <div class="input-group input-group-name">
                    <span class="icon-input"><i class="fas fa-user"></i></span>
                    <?= $form->field($model, 'nombre')->textInput(['class' => 'form-control with-icon', 'placeholder' => 'Ingresa tu nombre'])->label(false) ?>
                </div>

                <div class="input-group">
                    <span class="icon-input"><i class="fas fa-user"></i></span>
                    <?= $form->field($model, 'apellidos')->textInput(['class' => 'form-control with-icon', 'placeholder' => 'Ingresa tus apellidos'])->label(false) ?>
                </div>

                <div class="input-group">
                    <span class="icon-input"><i class="fa-solid fa-at"></i></span>
                    <?= $form->field($model, 'correo')->textInput(['class' => 'form-control with-icon', 'placeholder' => 'correo@correo.com'])->label(false) ?>
                </div>

                <div class="input-group">
                    <span class="icon-input"><i class="fa-solid fa-unlock-keyhole"></i></span>
                    <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control with-icon', 'placeholder' => 'Ingresa tu contraseña'])->label(false) ?>
                </div>

                <div class="input-group">
                    <span class="icon-input"><i class="fa-solid fa-lock"></i></span>
                    <?= $form->field($model, 'confirmarPassword')->passwordInput(['class' => 'form-control with-icon', 'placeholder' => 'Confirma tu contraseña'])->label(false) ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Registrar', ['class' => 'btn btn-clean btn-principal']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>

            <div class="col p-0">
                <div class="s-2 animate__animated animate__fadeIn">
                    <?php
                    // Ruta de la imagen dentro de la carpeta 'web/img'
                    $rutaImagen = Yii::getAlias('@web') . '/img/limpieza-playa.webp';
                    // Código HTML para mostrar la imagen
                    echo Html::img($rutaImagen, ['class' => 'img-register', 'alt' => 'Mantener un ecosistema limpio']);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>