<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\RegistroUsuarios $model */

$this->title = 'Update Registro Usuarios: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Registro Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="registro-usuarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
