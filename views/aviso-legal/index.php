<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Aviso Legal';
$this->params['breadcrumbs'][] = ['label' => 'Aviso Legal', 'url' => ['index']];
?>

<div class="aviso-legal">
    <div class="container">
        <div class="row my-5">
            <div class="col">
                <h1><?= Html::encode($this->title) ?></h1>

                <h5 class="mt-5">INFORMACIÓN GENERAL</h5>
                <p>El titular del sitio web www.ecoplayas.es FUNDACIÓN ECOPLAYAS . con NIF G12345678 y domicilio sitio en CALLE Prueba 3, 39001 Santander - CANTABRIA.</p>
                <p>
                    Con ánimo de que el uso del sitio web se ajuste a criterios de transparencia, claridad y sencillez, FUNDACIÓN ECOPLAYAS informa al usuario que cualquier sugerencia, duda o consulta sobre el presente Aviso Legal o cualquier otro documento informativo del sitio web será recibida y solucionada contactando con FUNDACIÓN ECOPLAYAS. a través de la dirección de correo electrónica: infoecoplayas@gmail.com
                </p>

                <h5 class="mt-5">PROPIEDAD INTELECTUAL</h5>
                <p>
                    Todos los contenidos que se muestran en el presente sitio web, y en especial diseños, textos, imágenes, logos, iconos, nombres comerciales, marcas o cualquier otra información susceptible de utilización industrial y/o comercial están protegidos por los correspondientes derechos de autor, no permitiendo su reproducción, transmisión o registro de información salvo autorización previa del titular, FUNDACIÓN ECOPLAYAS. No obstante, el usuario podrá utilizar información facilitada para gestionar su pedido y los datos de contacto correspondientes.
                </p>

                <h5 class="mt-5">ENLACES</h5>
                <p>
                    FUNDACIÓN ECOPLAYAS. no asume ninguna responsabilidad sobre los enlaces hacía otros sitios o páginas web que, en su caso, pudieran incluirse en el mismo, ya que no tiene ningún tipo de control sobre los mismos, por lo que el usuario accede bajo su exclusiva responsabilidad al contenido y en las condiciones de uso que rijan en los mismos.
                </p>
            </div>
        </div>
    </div>
</div>