<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\UsuariosRecursos $model */

$this->title = 'Create Usuarios Recursos';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios Recursos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarios-recursos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
