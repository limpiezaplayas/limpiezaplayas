<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\UsuariosRecursos $model */

$this->title = 'Update Usuarios Recursos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios Recursos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usuarios-recursos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
