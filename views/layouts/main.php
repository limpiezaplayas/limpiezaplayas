<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Dropdown;


AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/img/logo.ico')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <title>
        <?= Html::encode($this->title) ?>EcoPlayas
    </title>
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <header id="header">
        <?php
        NavBar::begin([
            'options' => ['class' => 'navbar-expand-md navbar-global h-100']
        ]);

        /* CONTENEDOR DEL LOGO */
        echo Html::a(
            '<img src="' . Yii::getAlias('@web/img/logo.png') . '" alt="Logo" class="logo">' .
                '<div class="text-container">' .
                '<div class="navbar-title">EcoPlayas</div>' .
                '<div class="navbar-subtitle">Voluntariado</div>' .
                '</div>',
            Yii::$app->homeUrl,
            ['class' => 'navbar-brand']
        );

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                [
                    'label' => 'Inicio',
                    'url' => ['/site/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Quiénes Somos',
                    'url' => ['/quienes-somos/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Donación',
                    'url' => ['/donaciones/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],

                [
                    'label' => 'Eventos',
                    'url' => ['/eventos/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Blog',
                    'url' => ['/blog/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],

                /*[
                            'label' => 'Playas',
                            'url' => ['/playas/index'],
                            'options' => ['class' => 'animate__animated animate__fadeIn']
                        ],*/

                [
                    'label' => 'Recursos Educativos',
                    'url' => ['/recursos-educativos/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],
            ],
        ]);

        echo '<div class="container-login-registro">'; // Abre el contenedor div para los elementos Login y Registrar

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login'], 'options' => ['class' => 'btn-login animate__animated animate__fadeIn']]
                ) : (
                    '<li class="nav-item dropdown">'
                    . Html::beginTag('a', [
                        'class' => 'nav-link dropdown-toggle container-perfil-img',
                        'href' => '#',
                        'id' => 'navbarDropdown',
                        'role' => 'button',
                        'data-toggle' => 'dropdown',
                        'aria-haspopup' => 'true',
                        'aria-expanded' => 'false'
                    ])
                    . Html::img(Yii::$app->user->identity->foto ? Yii::getAlias('@web') . '/' . Yii::$app->user->identity->foto : 'https://via.placeholder.com/40', [
                        'class' => 'img-circle img-perfil-header',
                        'alt' => 'User Image',
                        'style' => 'width: 40px; height: 40px; border-radius: 50%;'
                    ])
                    . ' ' . Yii::$app->user->identity->nombre
                    . Html::endTag('a')
                    . Dropdown::widget([
                        'items' => [
                            [
                                'label' => 'Ver perfil',
                                'url' => ['/actualizar-perfil/index', 'id' => Yii::$app->user->identity->id]
                            ],
                            '<div class="dropdown-divider"></div>',
                            Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    'Logout',
                                    ['class' => 'dropdown-item']
                                )
                                . Html::endForm(),
                        ],
                        'options' => ['class' => 'dropdown-menu', 'aria-labelledby' => 'navbarDropdown']
                    ])
                    . '</li>'
                ),
                [
                    'label' => 'Registrar',
                    'url' => ['/registro-usuarios/create'],
                    'options' => ['class' => 'animate__animated animate__fadeIn btn-registrar']
                ],
            ],
        ]);



        echo '</div>'; // Cierra el contenedor div para los elementos Login y Registrar

        NavBar::end();
        ?>
        </div>
    </header>








    <main id="main" class="main" role="main">
        <div class="container container-principal">
            <?php if (!empty($this->params['breadcrumbs'])) : ?>
                <?=
                Breadcrumbs::widget([
                    'links' => $this->params['breadcrumbs'],
                    'options' => ['class' => 'bread-general']
                ])
                ?>
            <?php endif ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>


    <footer id="footer" class="footer">
        <div class="container">
            <div class="row">
                <div class="col-4 footer-widget footer-logo">
                    <?php
                    echo Html::a(
                        '<img src="' . Yii::getAlias('@web/img/logo.png') . '" alt="Logo" class="logo">' .
                            '<div class="text-container">' .
                            '<div class="navbar-title">EcoPlayas</div>' .
                            '<div class="navbar-subtitle">Voluntariado</div>' .
                            '</div>',
                        Yii::$app->homeUrl,
                        ['class' => 'navbar-brand']
                    );
                    ?>
                </div>
                <div class="col-5 d-flex flex-column">
                    <span class="title">Contacto</span>
                    <div class="links-contact d-flex flex-column">
                        <a href="https://maps.app.goo.gl/Zwn3A9uUCvTLbJks7" target="_blank">
                            Calle Vargas 65, 39010 Santander, Cantabria
                        </a>

                        <a href="tel:987654321" target="_blank">
                            987 654 321
                        </a>
                        <a href="mailto:ecoplayass@gmail.com" target="_blank">
                            ecoplayass@gmail.com
                        </a>
                    </div>

                </div>
                <div class="col-3 footer-social">
                    <span class="title">Social</span>
                    <ul class="list-social">
                        <?= Html::a(
                            '<i class="fa-brands fa-gitlab"></i>',
                            'https://gitlab.com/limpiezaplayas/limpiezaplayas',
                            [
                                'target' => '_blank',
                                'class' => 'github-icon'
                            ]
                        ) ?>
                        <li class="social">
                            <?= Html::a(
                                '<i class="fa-brands fa-linkedin-in"></i>',
                                'https://www.linkedin.com/in/christopherquirozmendivel/',
                                [
                                    'target' => '_blank',
                                    'class' => 'linkedin-icon'
                                ]
                            ) ?>
                        </li>
                        <li class="social">
                            <?= Html::a(
                                '<i class="fab fa-github"></i>',
                                'https://github.com/christopherqmendivel?tab=repositories',
                                [
                                    'target' => '_blank',
                                    'class' => 'github-icon'
                                ]
                            ) ?>
                        </li>
                        <li class="social">
                            <?= Html::a(
                                '<i class="fa-brands fa-twitter"></i>',
                                'https://github.com/',
                                [
                                    'target' => '_blank',
                                    'class' => 'github-icon'
                                ]
                            ) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row line-copyright p-0"></div>

        <div class="container">
            <div class="row text-copyright">
                <div class="col-6">
                    <span class="text-white text-copyright">
                        Copyright © <?= Yii::$app->formatter->asDate(time(), 'yyyy') ?> - EcoPlayas

                    </span>
                </div>

                <div class="col-6 terms">
                    <?= Html::a(
                        'Aviso Legal',
                        ['/aviso-legal/index'],
                        [
                            'class' => 'terms'
                        ]
                    ) ?>

                    <?= Html::a(
                        'Política de privacidad',
                        ['/politica-privacidad/index'],
                        [
                            'class' => 'terms'
                        ]
                    ) ?>

                    <?= Html::a(
                        'Política de cookies',
                        ['/politica-cookies/index'],
                        [
                            'class' => 'terms'
                        ]
                    ) ?>
                </div>
            </div>
        </div>

    </footer>

    <?php $this->endBody() ?>



</body>

</html>
<?php $this->endPage() ?>