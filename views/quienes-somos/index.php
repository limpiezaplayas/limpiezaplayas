<?php

use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this yii\web\View */

$this->title = 'Quiénes Somos';


AppAsset::register($this);

?>

<div class="container p-0 container-about">
    <!-- SECTION 1 -->
    <div class="row s-1 d-flex justify-content-center">
        <div class="max-w d-flex justify-content-center">
            <div class="col-2 sect-1">
                <?= Html::img('@web/img/shape1.svg', ['alt' => 'Ejemplo', 'class' => 's-1-img']) ?>
            </div>
            <div class="col-6 sect-2">
                <h1> Potenciando el <span class="cambio">cambio</span> juntos</h1>
                <p class="text-center">
                    Únete a nuestra misión de mantener las playas limpias. Haz la diferencia y preserva la belleza natural de nuestros océanos. Únete a nuestra comunidad comprometida con la limpieza costera.
                </p>
            </div>
            <div class="col-2 sect-3">
                <?= Html::img('@web/img/shape2.svg', ['alt' => 'Ejemplo', 'class' => 's-1-img s-2-img']) ?>

            </div>
        </div>
    </div>

    <!-- SECTION 2 -->
    <div class="row row-2">
        <div class="col-6 sec-2">
            <h6 class="title">Qué hacemos</h6>
            <h2 class="subtitle">Descubra nuestro enfoque impulsado por la misión</h2>
        </div>

        <div class="col-6 list-row">
            <div class="contenedor-item w-100">
                <div class="list-items d-flex">
                    <div class="item seleccionado" id="mision">Misión</div>
                    <div class="item" id="trabajo">Trabajo en equipo</div>
                    <div class="item" id="integridad">Integridad</div>
                </div>

                <div class="info-text">

                    <div class="texto-info mostrar" id="texto-mision">
                        <p>
                            Mantener nuestras playas limpias y seguras mediante la participación activa de voluntarios comprometidos con la conservación del medio ambiente costero.
                        </p>
                    </div>
                    <div class="texto-info" id="texto-trabajo">
                        <p>

                            Unidos en el voluntariado de playa, cuidamos nuestras costas con pasión. Compartimos sonrisas, esfuerzos y la gratitud de contribuir al bienestar de nuestro planeta.
                        </p>
                    </div>

                    <div class="texto-info" id="texto-integridad">

                        Nos mantenemos auténticos y honestos en cada acción. Nos esforzamos por preservar nuestros entornos costeros, construyendo relaciones de confianza en nuestra comunidad.
                    </div>

                </div>
            </div>

        </div>

    </div>

    <!-- SECTION 3 -->

    <div class="cont-ab row-3">
        <div class="row one">
            <!-- FILA 1 -->
            <div class="col-md-4 c-logos">
                <?= Html::img('@web/img/logos/black/logo-marsh.png', ['alt' => 'Logo 1', 'class' => 'logo-w']) ?>
            </div>
            <div class="col-md-4 c-logos">
                <?= Html::img('@web/img/logos/black/logo-rimac.png', ['alt' => 'Logo 2', 'class' => 'logo-w']) ?>
            </div>
            <div class="col-md-4 c-logos">
                <?= Html::img('@web/img/logos/black/logo-randstad.png', ['alt' => 'Logo 6', 'class' => 'logo-w']) ?>
            </div>
        </div>

        <!-- FILA 2 -->
        <div class="row two">
            <div class="col-md-4 c-logos">
                <?= Html::img('@web/img/logos/black/logo-cic.png', ['alt' => 'Logo 4', 'class' => 'logo-w']) ?>
            </div>
            <div class="col-md-4 c-logos">
                <?= Html::img('@web/img/logos/black/logo-tj.png', ['alt' => 'Logo 5', 'class' => 'logo-w']) ?>
            </div>
            <div class="col-md-4 c-logos">
                <?= Html::img('@web/img/logos/black/axpe-logo.png', ['alt' => 'Logo 3', 'class' => 'logo-w']) ?>
            </div>
        </div>
    </div>


    <!-- SECTION 4 -->
    <div class="row row-4">
        <div class="max-w w-100 d-flex">
            <div class="col-6 sec-2">
                <h6 class="title">Por qué elegirnos</h6>
                <h2 class="subtitle mb-4">La opción confiable y certera para cuidar nuestras playas.</h2>
                <p>
                    Nuestra dedicación y compromiso te aseguran un cuidado óptimo de nuestras playas.
                </p>

                <div class="c-enlaces">
                    <div class="enlaces">
                        <div class="item item-1">
                            <a href="#voluntariado-en-playa" class="link-dropdown">1. Voluntariado en Playa</a>

                            <p class="texto-desplegable">Conoce más sobre el impacto ambiental en nuestras playas y cómo tu voluntariado puede marcar la diferencia en la preservación de la vida marina y los ecosistemas costeros.</p>
                        </div>
                        <div class="item item-2">
                            <a href="#impacto-ambiental" class="link-dropdown">2. Impacto Ambiental</a>
                            <p class="texto-desplegable">Descubre cómo puedes contribuir al cuidado de nuestras costas y participar en actividades de limpieza y conservación.</p>
                        </div>
                        <div class="item item-3">
                            <a href="#proyectos-actuales" class="link-dropdown">3. Proyectos Actuales</a>
                            <p class="texto-desplegable">Explora nuestros proyectos actuales de conservación costera y únete a nosotros para hacer del mundo un lugar mejor para las generaciones futuras.</p>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-6 sec-2">
                <?= Html::img('@web/img/voluntariado.png', ['alt' => 'Logo 3', 'class' => 'img logo-w w-100 h-100']) ?>
            </div>
        </div>
    </div>

</div>
</div>