<?php

use yii\helpers\Html;
use app\models\Usuarios; // Asegúrate de importar el modelo Usuarios si aún no lo has hecho

/** @var yii\web\View $this */
/** @var app\models\Usuarios $model */ // Cambia a Usuarios en lugar de CrearUsuario

$this->title = 'Crear usuario';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
