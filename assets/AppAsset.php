<?php
// assets/AppAsset.php

namespace app\assets;


use yii\web\AssetBundle;
use yii\web\View;


class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css',

        // CDN A FONTAWESOME ICONS
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css',

        // Animate css
        'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css',

        // CDN BOOTSTRAP 4
        'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css',


        // FUENTE CAVEAT
        'https://fonts.googleapis.com/css2?family=Caveat:wght@400..700&display=swap',

        // FUENTE - PARRAFOS
        'https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100..900;1,100..900&display=swap',



    ];

    public $js = [
        'js/script.js',

        // PARALLAX
        'https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js',

    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];

    /**
     * Register React bundle
     */
    public function registerReactBundle($view)
    {
        $view->registerJsFile($this->baseUrl . '/js/bundle.js', ['position' => View::POS_END]);
    }
}
